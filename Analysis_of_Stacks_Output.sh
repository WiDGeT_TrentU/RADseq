Analysis_of_Stacks_Output.sh
############################################################################################################################################
#Summary Statistics
	#In a spreadsheet with the following headings for the columns:
		#Method
		#Parameters
		#Reads_used
		#Completion
		#num_loci
		#num_SNPs
		#mean_FIS
		#mean_het
		#mean_pi
		#R^2 from HO vs HE per SNP,
	#use the following instrucutions to obtain the information:
		#Method
			#either reference-based or denovo
		#Parameters
			#for example, in denovo you can change the following
				#in ustacks module:
					#-m: minimum depth of coverage required to create a stack (default: 3)
					#-M: maximum distance (in nucleotides) allowed between stacks (default: 2)
				#in cstacks module:
					#-n: number of mismatches allowed between sample loci when build the catalog (default: 1)
		#Reads_used
			#PE_mapped: reference-based; paired-end mapped to a reference genome
			#PE_with_tsv2bam: denovo; paired-end using tsv2bam
		#Completion
			#in populations module
				#-r: minimum percentage of individuals in a population required to process a locus for that population (aka do all individuals in the population need that locus in order to include it in the analysis?)
		#num_loci
			#Number of loci kept after filtering through the populations module
			#in the directory with the output from the populations module,
			grep "Kept" populations.log
		#num_SNPs
			#Number of variant sites kept after filtering through populations module
			#if you are using the flag --write_single_snp, it will only count the first SNP per locus. This is more efficient as SNPs on the same locus are almost always inherited together and therefore will not show any relevent differences between individuals.
			#in the directory with the output from the populations module,
			grep "variant sites remained" populations.log
		#mean_FIS
			#the inbreeding coefficient; 0 indicates the population is completely outbred, 1 indicates the population is completely inbred
			#in the directory with the output from the populations module,
			vcftools --vcf populations.snps.vcf --het #this creates the files out.log and out.het
			cut -f5 out.het | sed 1d > out.fis.txt #the inbreeding coefficient per individual | deletes the first line of text (aka the headers)
			#winSCP out.fis.txt to computer
			#now in R
			setwd("~/working_directory") #to where you copied the file to your computer
			fis=read.table("out.fis.txt")
			mean(fis$V1)
			#or if you don't want to use R do the following. However, this output can be different by 0.01
			#in the directory with the output from the populations module,
			vcftools --vcf populations.snps.vcf --het #this creates the files out.log and out.het
			cut -f5 out.het | awk '{s+=$1}END{print "ave:",s/NR}'
		#mean_het
			#a measure of heterozygosity
			#in the directory with the output from the populations module,
			vcftools --vcf populations.snps.vcf --het #this creates the files out.log and out.het
			cut -f2 out.het | sed 1d > out.homo.txt #observed number of homozygous sites per individual | deletes the first line of text (aka the headers)
			cut -f4 out.het | sed 1d > out.sites.txt #total number of sites per individual | deletes the first line of text (aka the headers)
			#winSCP out.homo.txt and out.sites.txt to computer
			#now in R
			setwd("~/working_directory") #to where you copied the files to your computer
			homo=read.table("out.homo.txt")
			sites=read.table("out.sites.txt")
			mean(1-(homo$V1/sites$V1))
		#mean_pi
			#a measure of nucleotide diversity or degree of polymorphism within a population
			#in the directory with the output from the populations module,
			vcftools --vcf populations.snps.vcf --site-pi #this creates the files out.log and out.sites.pi
			cut -f3 out.sites.pi | sed 1d > out.pi.txt #nucleotide diversity per individual | deletes the first line of the text (aka the headers)
			#winSCP out.pi.txt to computer
			#now in R
			setwd("~/working_directory") #to where you copied the file to your computer
			pi=read.table("out.pi.txt")
			mean(pi$V1)
			#or if you don't want to use R do the following. However, this output can be different by 0.0001
			#in the directory with the output from the populations module,
			vcftools --vcf populations.snps.vcf --site-pi # this creates the files out.log and out.sites.pi
			cut -f3 out.sites.pi | awk '{s+=$1}END{print "ave:",s/NR}'
		#R^2 from HO vs HE per SNP
			#the corrolation coefficient for the observed heterozygosity versus the expected heterozygosity
			#in the directory with the output from the populations module,
			vcftools --vcf populations.snps.vcf --hardy #this creates the file out.hwe
			cut -f3,4 out.hwe | awk '{gsub("/","\t",$0); print;}' | sed 1d > HO_HE.txt
			#WinSCP HO_HE.txt to computer
			#in R
			setwd("~/working_directory") #to where you copied the file to your computer
			hwe=read.table("HO_HE.txt")
			HO <- (hwe$V2)/(hwe$V1+hwe$V2+hwe$V3)
			HE <- (hwe$V5)/(hwe$V4+hwe$V5+hwe$V6)
			plot(HE, HO, 
			     main = "Observed Heterozygosity vs Expected Heterozygosity",
			     xlab = "Expected Heterozygosity",
			     ylab = "Observed Heterozygosity",
			     cex.main=0.80)
			abline(a=0, b=1, col="red")
			reg <- lm(HO~HE, data = hwe)
			modsum=summary(reg)
			modsum$adj.r.squared
############################################################################################################################################
#FastStructure
	#to turn a vcf file into a structure file and run fastStructure to estimate number of populations, see:
	https://gitlab.com/WiDGeT_TrentU/Tutorials/blob/1a865d79eabe9729b04923dd35d5800e718af6f6/fastStructure_SNPs/fastStructure.md
############################################################################################################################################
#Allele Frequency Distribution Plot and PCA
	#WinSCP "populations.snps.vcf" to your computer
	#in R
	library(vcfR)
	library(adegenet)
	library(adegraphics)
	library(pegas)
	library(StAMPP)
	library(lattice)
	library(gplots)
	library(ape)
	library(ggmap)
	setwd("~/working_directory") #to where you copied populations.snps.vcf
	vcf <- read.vcfR("populations.snps.vcf")
	vcf@fix[1:10,1:5]
	aa.genlight <- vcfR2genlight(vcf, n.cores=1)
	locNames(aa.genlight) <- paste(vcf@fix[,1],vcf@fix[,2],sep="_")
	pop(aa.genlight)<-substr(indNames(aa.genlight),1,3)
	aa.genlight
	indNames(aa.genlight)
	as.matrix(aa.genlight)[1:16,1:10]
	pop(aa.genlight)
	glPlot (aa.genlight)
	x <- summary(t(as.matrix(aa.genlight)))
	write.table(x[7,], file = "missing.persample.txt", sep = "\t")
	#Allele Frequency Distribution Plot
	mySum <- glSum(aa.genlight, alleleAsUnit = TRUE)
	AlleleFreqPlot <- barplot(table(mySum), col="blue", space=0, xlab="Allele counts",
	        main="Allele Frequency Distribution")
	#This next part just gets glPcaFast to work so that it doesn't take very long to make the PCA
		glPcaFast <- function(x,
		                      center=TRUE,
		                      scale=FALSE,
		                      nf=NULL,
		                      loadings=TRUE,
		                      alleleAsUnit=FALSE,
		                      returnDotProd=FALSE){
		  if(!inherits(x, "genlight")) stop("x is not a genlight object")
		  # keep the original mean / var code, as it's used further down
		  # and has some NA checks..
		  if(center) {
		    vecMeans <- glMean(x, alleleAsUnit=alleleAsUnit)
		    if(any(is.na(vecMeans))) stop("NAs detected in the vector of means")
		  }
		  if(scale){
		    vecVar <- glVar(x, alleleAsUnit=alleleAsUnit)
		    if(any(is.na(vecVar))) stop("NAs detected in the vector of variances")
		  }
		  # convert to full data, try to keep the NA handling as similar
		  # to the original as possible
		  # - dividing by ploidy keeps the NAs
		  mx <- t(sapply(x$gen, as.integer)) / ploidy(x)
		  # handle NAs
		  NAidx <- which(is.na(mx), arr.ind = T)
		  if (center) {
		    mx[NAidx] <- vecMeans[NAidx[,2]]
		  } else {
		    mx[NAidx] <- 0
		  }
		  # center and scale
		  mx <- scale(mx,
		              center = if (center) vecMeans else F,
		              scale = if (scale) vecVar else F)
		  # all dot products at once using underlying BLAS
		  # to support thousands of samples, this could be
		  # replaced by 'Truncated SVD', but it would require more changes
		  # in the code around
		  allProd <- tcrossprod(mx) / nInd(x) # assume uniform weights
		  ## PERFORM THE ANALYSIS ##
		  ## eigenanalysis
		  eigRes <- eigen(allProd, symmetric=TRUE, only.values=FALSE)
		  rank <- sum(eigRes$values > 1e-12)
		  eigRes$values <- eigRes$values[1:rank]
		  eigRes$vectors <- eigRes$vectors[, 1:rank, drop=FALSE]
		  ## scan nb of axes retained
		  if(is.null(nf)){
		    barplot(eigRes$values, main="Eigenvalues", col=heat.colors(rank))
		    cat("Select the number of axes: ")
		    nf <- as.integer(readLines(n = 1))
		  }
		  ## rescale PCs
		  res <- list()
		  res$eig <- eigRes$values
		  nf <- min(nf, sum(res$eig>1e-10))
		  ##res$matprod <- allProd # for debugging
		  ## use: li = XQU = V\Lambda^(1/2)
		  eigRes$vectors <- eigRes$vectors * sqrt(nInd(x)) # D-normalize vectors
		  res$scores <- sweep(eigRes$vectors[, 1:nf, drop=FALSE],2,
		                      sqrt(eigRes$values[1:nf]), FUN="*")
		  ## GET LOADINGS ##
		  ## need to decompose X^TDV into a sum of n matrices of dim p*r
		  ## but only two such matrices are represented at a time
		  if(loadings){
		    if(scale) {
		      vecSd <- sqrt(vecVar)
		    }
		    res$loadings <- matrix(0, nrow=nLoc(x), ncol=nf) # create empty matrix
		    ## use: c1 = X^TDV
		    ## and X^TV = A_1 + ... + A_n
		    ## with A_k = X_[k-]^T v[k-]
		    myPloidy <- ploidy(x)
		    for(k in 1:nInd(x)){
		      temp <- as.integer(x@gen[[k]]) / myPloidy[k]
		      if(center) {
		        temp[is.na(temp)] <- vecMeans[is.na(temp)]
		        temp <- temp - vecMeans
		      } else {
		        temp[is.na(temp)] <- 0
		      }
		      if(scale){
		        temp <- temp/vecSd
		      }
		      res$loadings <- res$loadings + matrix(temp) %*% eigRes$vectors[k,
		                                                                     1:nf, drop=FALSE]
		    }
		    res$loadings <- res$loadings / nInd(x) # don't forget the /n of X_tDV
		    res$loadings <- sweep(res$loadings, 2, sqrt(eigRes$values[1:nf]),
		                          FUN="/")
		  }
		  ## FORMAT OUTPUT ##
		  colnames(res$scores) <- paste("PC", 1:nf, sep="")
		  if(!is.null(indNames(x))){
		    rownames(res$scores) <- indNames(x)
		  } else {
		    rownames(res$scores) <- 1:nInd(x)
		  }
		  if(!is.null(res$loadings)){
		    colnames(res$loadings) <- paste("Axis", 1:nf, sep="")
		    if(!is.null(locNames(x)) & !is.null(alleles(x))){
		      rownames(res$loadings) <- paste(locNames(x),alleles(x), sep=".")
		    } else {
		      rownames(res$loadings) <- 1:nLoc(x)
		    }
		  }
		  if(returnDotProd){
		    res$dotProd <- allProd
		    rownames(res$dotProd) <- colnames(res$dotProd) <- indNames(x)
		  }
		  res$call <- match.call()
		  class(res) <- "glPca"
		  return(res)
		}
	#Make PCA
	pca.1 <- glPcaFast(aa.genlight, nf=300)
	#Visualize it
	scatter.glPca(pca.1, xax = 1, yax = 2)

	#for explanations to the R code for Allele Frequency Plots and PCAs see:
	https://botany.natur.cuni.cz/hodnocenidat/Lesson_05_tutorial.pdf

#End