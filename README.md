```ddRAD / RADseq Scripts```

<b>Full ddRAD analytical pipeline from:<b>

Shafer ABA, Peart C, Tusso S, Maayan I, Brelsford A, Wheat C, Wolf JBW (2017) Bioinformatic processing of RAD-seq data dramatically impacts downstream population genetic inference. Methods in Ecology and Evolution 8: 907-917

1. Shafer_et.al_2017_MEE_supplemental_material_scripts_1.txt

2. Shafer_et.al_2017_MEE_supplemental_material_scripts_2.txt

<b>Full ddRAD analytical pipeline from:<b>

Haworth SE*, Nituch L, Northrup JM, Shafer ABA (In Press) Characterizing the demographic history and prion protein variation to infer susceptibility to chronic wasting disease in a naïve population of white-tailed deer (Odocoileus virginianus). Evolutionary Applications doi: 10.1111/eva.13214

<b>Updated STACKs pipeline for use on TrentU server<b>

1. Stacks_reference_based_protocol.sh

2. Stacks_denovo_based_protocol.sh

3. Analysis_of_Stacks_Output.sh

<b>epiGBS protocol from:<b>

Meröndun JB*, Murray DL, Shafer ABA (2019) Genome-scale sampling suggests cryptic epigenetic structuring and insular divergence in Canada lynx. Molecular Ecology 28:3186–3196 (BioRxiv 316711)

1. Johnson_et.al_2018_UNIX_R.txt
