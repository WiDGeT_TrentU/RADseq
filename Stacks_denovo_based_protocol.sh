Stacks_denovo_based_protocol.sh

#Note: in this protocol, the examples use ddRADseq data that consists of 4 index-barcodes and 24 inline-barcodes.

#in your home directory you should have a folder (in this case "ddRADseq") with the ddRADseq gzfastq files
 	#These files have already been partially demultiplexed by the sequencing company by processing the index-barcodes.
 	#They should look something like this with 2 reads for each of the index-barcodes.
		#ddRAD1_AATCCG_R1.fastq.gz
		#ddRAD1_AATCCG_R2.fastq.gz
		#ddRAD1_ACTGGT_R1.fastq.gz
		#ddRAD1_ACTGGT_R2.fastq.gz
#you now have to use process_radtags to demultiplex the inline-barcodes.	

#in your home directory
mkdir Working
cd Working
mkdir samples
#in "Working" folder you should have:
	#a barcodes file with the 24 inline barcodes each on a new line,
	#an empty "samples" folder to put the demultiplexed files,
	#symbolic links to the executables for Stacks (process_radtags, ustacks, cstacks, sstacks, tsv2bam, gstacks, and populations)

#copy the R1 and R2 files from the first index-barcode into your "Working" folder
#for example, if AATCCG is the first index-barcode,
#in "Working"
cp ../ddRADseq/ddRAD1_AATCCG_R1.fastq.gz ./ &
cp ../ddRADseq/ddRAD1_AATCCG_R2.fastq.gz ./ &
#demultiplex the first 2 files
process_radtags -1 ddRAD1_AATCCG_R1.fastq.gz -2 ddRAD1_AATCCG_R2.fastq.gz -o ./samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq &
	#-1 is the first of 2 paired files
	#-2 is the second of 2 paired files
	#-o is the output destination, a folder called "samples"
	#-b the barcodes are in a file called "barcodes".
	#-e specifies the enzyme used was "SbfI"
	#-E specifies that the quality scores are encoded as "phred33"
	#-r rescues barcodes and RADtags
	#-c cleans the data to remove reads with uncalled bases
	#-q gets rid of reads with low quality scores (the threshold is not specified)
	#-i indicates the input files are in gzfastq format
cd samples
#the number of output files for each index-barcode will be 4x(the number of inline-barcodes), in this case 4x24=96.
	#the 4 files for each inline-barcode will look something like this (AACGAA is the first inline-barcode):
		#sample_AACGAA.1.fq.gz
		#sample_AACGAA.2.fq.gz
		#sample_AACGAA.rem.1.fq.gz
		#sample_AACGAA.rem.2.fq.gz
	#the first 2 are successfully demultiplexed reads 1 and 2.
	#The .rem files contain good quality data however the program was not able to find the pair for the reads.
#rename the files to the ID of the individual to ensure they won't get overwritten when you demultiplex the next index-barcode.
rename 's/sample_AACGAA./RR_01./' *
rename 's/sample_AACTCG./RR_02./' *
rename 's/sample_AAGATA./RR_03./' *
rename 's/sample_ACCAGA./RR_04./' *
#etc for all 24 inline-barcodes
#also rename the log file
mv process_radtags.log process_radtags1.log
#delete the gzfastq files from "Working" so you can do the next index-barcode
cd ../
rm ddRAD1_*
#repeat this process by copying the next 2 files into "Working", running process_radtags, and renaming the files until all samples have been demultiplexed.
#Note: process_radtags is capable of demultiplexing a combination of index and inline-barcodes. If the sequencing company didn't partially demultiplex the data, there would be no need to copy the files and rename them. You would simply give the path to the folder containing the RADseq data and give the flag to say it is paired.

#Now that the samples are demultiplexed, you can use the individual Stacks modules to assemble and catalog loci, and call SNPs
#the .rem files won't be used in the denovo analysis but it is fine to leave them there
cd ..
#in "Working"
mkdir output_denovo
cd samples
#you will be using just R1 for now but you will pair them together later
nano ustacks.sh #ustacks assembles reads denovo. There are many flags you can add to change parameters and filter the data. see http://catchenlab.life.illinois.edu/stacks/comp/ustacks.php
	#!/bin/bash
	#in "samples" folder
	ustacks -f ./RR_01.1.fq.gz -o ../output_denovo -p 8 -t gzfastq -i 1 --name RR_01
	ustacks -f ./RR_02.1.fq.gz -o ../output_denovo -p 8 -t gzfastq -i 2 --name RR_02
	ustacks -f ./RR_03.1.fq.gz -o ../output_denovo -p 8 -t gzfastq -i 3 --name RR_03
	#etc
bash ustacks.sh &

#Meanwhile,
#Population Map
cd ..
#This can be done while the commands above are running.
#Make a population map to give to the rest of the modules.
#This is a text document with the file names (without the .1.bam) in one column and a population ID (as a number) in the second column.
#It should be tab deliminated and no header. For example:
#in "Working"
nano popmap
	RR_01	3
	RR_02	4
	RR_03	2
	#etc

#Once all that is done,
cd output_denovo
cstacks -P ./ -M ../popmap -p 8 & #this creates a catalog of loci
sstacks -P ./ -M ../popmap -p 8 & #this matchs all individuals against the catalog, creating a map of alleles
tsv2bam -P ./ -M ../popmap -R ../samples -t 8 & #this pairs R2 to R1
gstacks -P ./ -M ../popmap -t 8 & #this assembles and merges paired-end contigs, calls SNPs in the populations, and genotypes each sample
#you can run populations many times to change the filters. For example,
populations -P ./ -M ../popmap -t 8 -r 0.5 --vcf --structure --write_single_snp --ordered_export &
#see http://catchenlab.life.illinois.edu/stacks/comp/populations.php for more flags and what they do.

#for information about how to use and analyze the output files please see:
https://gitlab.com/WiDGeT_TrentU/RADseq/blob/master/Analysis_of_Stacks_Output.sh

#End