Stacks_reference_based_protocol.sh

#Note: in this protocol, the examples use ddRADseq data that consists of 4 index-barcodes and 24 inline-barcodes.

#in your home directory you should have a folder (in this case "ddRADseq") with the ddRADseq gzfastq files
 	#These files have already been partially demultiplexed by the sequencing company by processing the index-barcodes.
 	#They should look something like this with 2 reads for each of the index-barcodes.
		#ddRAD1_AATCCG_R1.fastq.gz
		#ddRAD1_AATCCG_R2.fastq.gz
		#ddRAD1_ACTGGT_R1.fastq.gz
		#ddRAD1_ACTGGT_R2.fastq.gz
#you now have to use process_radtags to demultiplex the inline-barcodes.	

#in your home directory
mkdir Working
cd Working
mkdir samples
mkdir reference_genome
#in "Working" folder you should have:
	#a barcodes file with the 24 inline barcodes each on a new line,
	#an empty "samples" folder to put the demultiplexed files,
	#a "reference_genome" folder with your reference genome (in fasta format) and,
	#symbolic links to the executables for Stacks (process_radtags and ref_map.pl)

#copy the R1 and R2 files from the first index-barcode into your "Working" folder
#for example, if AATCCG is the first index-barcode,
#in "Working"
cp ../ddRADseq/ddRAD1_AATCCG_R1.fastq.gz ./ &
cp ../ddRADseq/ddRAD1_AATCCG_R2.fastq.gz ./ &
#demultiplex the first 2 files
process_radtags -1 ddRAD1_AATCCG_R1.fastq.gz -2 ddRAD1_AATCCG_R2.fastq.gz -o ./samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq &
	#-1 is the first of 2 paired files
	#-2 is the second of 2 paired files
	#-o is the output destination, a folder called "samples"
	#-b the barcodes are in a file called "barcodes".
	#-e specifies the enzyme used was "SbfI"
	#-E specifies that the quality scores are encoded as "phred33"
	#-r rescues barcodes and RADtags
	#-c cleans the data to remove reads with uncalled bases
	#-q gets rid of reads with low quality scores (the threshold is not specified)
	#-i indicates the input files are in gzfastq format
cd samples
#the number of output files for each index-barcode will be 4x(the number of inline-barcodes), in this case 4x24=96.
	#the 4 files for each inline-barcode will look something like this (AACGAA is the first inline-barcode):
		#sample_AACGAA.1.fq.gz
		#sample_AACGAA.2.fq.gz
		#sample_AACGAA.rem.1.fq.gz
		#sample_AACGAA.rem.2.fq.gz
	#the first 2 are successfully demultiplexed reads 1 and 2.
	#The .rem files contain good quality data however the program was not able to find the pair for the reads.
#rename the files to the ID of the individual to ensure they won't get overwritten when you demultiplex the next index-barcode.
rename 's/sample_AACGAA./RR_01./' *
rename 's/sample_AACTCG./RR_02./' *
rename 's/sample_AAGATA./RR_03./' *
rename 's/sample_ACCAGA./RR_04./' *
#etc for all 24 inline-barcodes
#also rename the log file
mv process_radtags.log process_radtags1.log
#delete the gzfastq files from "Working" so you can do the next index-barcode
cd ../
rm ddRAD1_*
#repeat this process by copying the next 2 files into "Working", running process_radtags, and renaming the files until all samples have been demultiplexed.
#Note: process_radtags is capable of demultiplexing a combination of index and inline-barcodes. If the sequencing company didn't partially demultiplex the data, there would be no need to copy the files and rename them. You would simply give the path to the folder containing the RADseq data and give the flag to say it is paired.

#Now that the samples are demultiplexed the reference genome needs to be prepared for mapping.
cd reference_genome
#index the genome (this takes a long time)
bwa index -a bwtsw genome.fasta
#generate the fasta file index
samtools faidx genome.fasta
#generate sequence dictionary
#create a symbolic link to picard.jar in "reference_genome", then
java -jar picard.jar CreateSequenceDictionary \
REFERENCE= genome.fasta \
OUTPUT= genome.dict

cd ..
#Next, use 3 separate "for" loops to map the paired and .rem files to the genome (these can be done simultaniously but still takes ~3.5 h)
#You may want to skip to making the population map while this is running in the background.
#in "Working"
mkdir mapped_reads
cd samples
#for the paired files
for f in `ls *.fq.gz | cut -f1 -d'.'| uniq`
do
bwa mem -t 16 ../reference_genome/genome.fasta ${f}.1.fq.gz ${f}.2.fq.gz > ../mapped_reads/${f}.paired.sam
done &
#for the .rem.1 files
for f in `ls *.fq.gz | cut -f1 -d'.'| uniq`
do
bwa mem -t 16 ../reference_genome/genome.fasta ${f}.rem.1.fq.gz > ../mapped_reads/${f}.rem.1.sam
done &
#for the .rem.2 files
for f in `ls *.fq.gz | cut -f1 -d'.'| uniq`
do
bwa mem -t 16 ../reference_genome/genome.fasta ${f}.rem.2.fq.gz > ../mapped_reads/${f}.rem.2.sam
done &

#once that is complete,
cd ../mapped_reads
#convert SAM to BAM
#in "mapped_reads" folder
for f in `ls *.paired.sam | cut -f1 -d'.'| uniq`
do
samtools view -bT ../reference_genome/genome.fasta ${f}.paired.sam > ${f}.paired.bam
done &

for f in `ls *.rem.1.sam | cut -f1 -d'.'| uniq`
do
samtools view -bT ../reference_genome/genome.fasta ${f}.rem.1.sam > ${f}.rem.1.bam
done &

for f in `ls *.rem.2.sam | cut -f1 -d'.'| uniq`
do
samtools view -bT ../reference_genome/genome.fasta ${f}.rem.2.sam > ${f}.rem.2.bam
done &

#Next,
#sort BAM files
#in "mapped_reads" folder
for f in `ls *.paired.bam | cut -f1 -d'.'| uniq`
do
samtools sort ${f}.paired.bam ${f}.sorted.paired
done &

for f in `ls *.rem.1.bam | cut -f1 -d'.'| uniq`
do
samtools sort ${f}.rem.1.bam ${f}.sorted.rem.1
done &

for f in `ls *.rem.2.bam | cut -f1 -d'.'| uniq`
do
samtools sort ${f}.rem.2.bam ${f}.sorted.rem.2
done &

#Next,
#make a BAM index
#in "mapped_reads" folder
for f in `ls *.sorted.paired.bam | cut -f1 -d'.'| uniq`
do
samtools index ${f}.sorted.paired.bam ${f}.sorted.paired.bai
done &

for f in `ls *.sorted.rem.1.bam | cut -f1 -d'.'| uniq`
do
samtools index ${f}.sorted.rem.1.bam ${f}.sorted.rem.1.bai
done &

for f in `ls *.sorted.rem.2.bam | cut -f1 -d'.'| uniq`
do
samtools index ${f}.sorted.rem.2.bam ${f}.sorted.rem.2.bai
done &

#Then,
#merge the three bam files into one
#in "mapped_reads" folder
mkdir ../final_mapped_reads
for f in `ls *.sorted.paired.bam | cut -f1 -d'.'| uniq`
do
samtools merge ../final_mapped_reads/${f}.bam ${f}.sorted.paired.bam ${f}.sorted.rem.1.bam ${f}.sorted.rem.2.bam
done &

#Meanwhile,
#Population Map
cd ..
#This can be done while the commands above are running.
#Make a population map to give to ref_map.pl.
#This is a text document with the file names (without the .bam) in one column and a population ID (as a number) in the second column.
#It should be tab deliminated and no header. For example:
#in "Working"
nano popmap
	RR_01	3
	RR_02	4
	RR_03	2
	RR_04	8
	#etc

#Finally,
#Ref_map.pl
#this is used to catalog loci and call SNPs
#in "Working" 
mkdir output_refmap
ref_map.pl --samples ./final_mapped_reads/ --popmap ./popmap -o ./output_refmap/ -X "populations: -r 0.5"
	#ref_map.pl consists of 2 modules, gstacks and populations.
	#you can pass different flags to the different modules through -X "module_name: -flag"
	#you can also run just populations many times to change the filters. For example,
populations -P ./ -O ./ -M ../popmap -t 8 -r 0.8 --write_single_snp --structure --ordered_export --vcf --genepop
#see http://catchenlab.life.illinois.edu/stacks/comp/ref_map.php for more flags and what they do.

#for information about how to use and analyze the output files please see:
https://gitlab.com/WiDGeT_TrentU/RADseq/blob/master/Analysis_of_Stacks_Output.sh

#End