#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP1_r080_gstacks
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
gstacks -I ./final_bams/ -M ./popmap1 -O ./pop1_r080_H/ -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP1_r080_populations
module load nixpkgs/16.09 gcc/7.3.0
module load stacks/2.3e
populations -P ./pop1_r080_H/ -M ./popmap1 -r 0.80 -H --vcf --genepop --fstats --smooth --hwe -t 16
#END


#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP1_r090_gstacks
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
gstacks -I ./final_bams/ -M ./popmap1 -O ./pop1_r090_H/ -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP1_r08_populations
module load nixpkgs/16.09 gcc/7.3.0
module load stacks/2.3e
populations -P ./pop1_r090_H/ -M ./popmap1 -r 0.90 -H --vcf --genepop --fstats --smooth --hwe -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP1_r095_gstacks
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
gstacks -I ./final_bams/ -M ./popmap1 -O ./pop1_r095_H/ -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP1_r095_populations
module load nixpkgs/16.09 gcc/7.3.0
module load stacks/2.3e
populations -P ./pop1_r095_H/ -M ./popmap1 -r 0.95 -H --vcf --genepop --fstats --smooth --hwe -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP1_r100_gstacks
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
gstacks -I ./final_bams/ -M ./popmap1 -O ./pop1_r100_H/ -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP1_r100_populations
module load nixpkgs/16.09 gcc/7.3.0
module load stacks/2.3e
populations -P ./pop1_r100_H/ -M ./popmap1 -r 1.00 -H --vcf --genepop --fstats --smooth --hwe -t 16
#END


#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP2_r080_gstacks
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
gstacks -I ./final_bams/ -M ./popmap3 -O ./pop3_r080/ -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP2_r080_populations
module load nixpkgs/16.09 gcc/7.3.0
module load stacks/2.3e
populations -P ./pop3_r080/ -M ./popmap3 -r 0.80 -H --vcf --genepop --fstats --smooth --hwe -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP2_r090_gstacks
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
gstacks -I ./final_bams/ -M ./popmap3 -O ./pop3_r090/ -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP2_r090_populations
module load nixpkgs/16.09 gcc/7.3.0
module load stacks/2.3e
populations -P ./pop3_r090/ -M ./popmap3 -r 0.90 -H --vcf --genepop --fstats --smooth --hwe -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP2_r095_gstacks
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
gstacks -I ./final_bams/ -M ./popmap3 -O ./pop3_r095/ -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP2_r095_populations
module load nixpkgs/16.09 gcc/7.3.0
module load stacks/2.3e
populations -P ./pop3_r095/ -M ./popmap3 -r 0.95 -H --vcf --genepop --fstats --smooth --hwe -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP2_r100_gstacks
module load nixpkgs/16.09  gcc/7.3.0
module load stacks/2.3e
gstacks -I ./final_bams/ -M ./popmap3 -O ./pop3_r100/ -t 16
#END

#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=03-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### POP2_r100_populations
module load nixpkgs/16.09 gcc/7.3.0
module load stacks/2.3e
populations -P ./pop3_r100/ -M ./popmap3 -r 1.00 -H --vcf --genepop --fstats --smooth --hwe -t 16
#END

