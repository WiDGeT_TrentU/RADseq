#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=02-07:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### J_index_genome
module load bwa
bwa index -a bwtsw wtdgenome1.fasta
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-30:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
module load bwa samtools
################### J_Align_RadSeq_PairedSamples
for f in `ls Pair-files/*.1.fq.gz | cut -f 1 -d'.'` 
do
bwa mem -t 16 wtdgenome1.fasta ${f}.1.fq.gz ${f}.2.fq.gz > ${f}.sam
################### create bam file from sam
samtools view -S -b -@ 16 ${f}.sam > ${f}.bam
################### sort the bam file
samtools sort -@ 16 ${f}.bam -o ${f}_sorted.bam
################### index the bam file
samtools index ${f}_sorted.bam
done
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-10:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
module load bwa samtools
################### J_Align_RadSeq_RemSamples
for f in `ls Rem-files/*.1.fq.gz | cut -f 1 -d'.'` 
do
bwa mem -t 16 wtdgenome1.fasta ${f}.rem.1.fq.gz > ${f}.rem.1.sam
bwa mem -t 16 wtdgenome1.fasta ${f}.rem.2.fq.gz > ${f}.rem.2.sam
################### create bam file from sam
samtools view -S -b -@ 16 ${f}.rem.1.sam > ${f}.rem.1.bam
samtools view -S -b -@ 16 ${f}.rem.2.sam > ${f}.rem.2.bam
################### sort the bam file
samtools sort -@ 16 ${f}.rem.1.bam -o ${f}_sorted.rem.1.bam
samtools sort -@ 16 ${f}.rem.2.bam -o ${f}_sorted.rem.2.bam
################### index the bam file
samtools index ${f}_sorted.rem.1.bam
samtools index ${f}_sorted.rem.2.bam
done
################### moving Files
cp Rem-files/* Bams
cp Pair-files/* Bams
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=16
#SBATCH --mem=64000
#SBATCH --time=0-10:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
module load bwa samtools
################### J_Combine_bams1
for f in `ls Bams/*.1.fq.gz | cut -f 1 -d'.'` 
do
samtools merge ${f}-final.bam ${f}_sorted.bam ${f}_sorted.rem.1.bam ${f}_sorted.rem.2.bam
done
################### moving all the merged, aligned, sorted bam files to final_bams directory
mv Bams/*-final.bam final_bams
################### renaming all the final bam files
for f in `ls final_bams/*-final.bam | cut -f 1 -d'-'` 
do
mv ${f}-final.bam ${f}.bam 
done
#END
