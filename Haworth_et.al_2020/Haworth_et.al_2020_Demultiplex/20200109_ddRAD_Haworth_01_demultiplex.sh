#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_01
module load stacks

process_radtags -1 Pool1_SEH_AATCCG_S3_L001_R1_001.fastq.gz -2 Pool1_SEH_AATCCG_S3_L001_R2_001.fastq.gz -o ./Pool1_SEH_AATCCG_S3_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool1_SEH_AATCCG_S3_samples/

mv sample_AACGAA.1.fq.gz NOO_1159.1.fq.gz
mv sample_AACGAA.2.fq.gz NOO_1159.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz NOO_1159.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz NOO_1159.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz SWO_1204.1.fq.gz
mv sample_AACTCG.2.fq.gz SWO_1204.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz SWO_1204.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz SWO_1204.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz NOO_1000.1.fq.gz
mv sample_AAGATA.2.fq.gz NOO_1000.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz NOO_1000.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz NOO_1000.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz NOO_1234.1.fq.gz
mv sample_ACCAGA.2.fq.gz NOO_1234.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz NOO_1234.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz NOO_1234.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz NOO_1040.1.fq.gz
mv sample_ACTTCC.2.fq.gz NOO_1040.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz NOO_1040.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz NOO_1040.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz NOO_0927.1.fq.gz
mv sample_ATCGTC.2.fq.gz NOO_0927.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz NOO_0927.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz NOO_0927.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz SEO_1199.1.fq.gz
mv sample_ATTCAT.2.fq.gz SEO_1199.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz SEO_1199.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz SEO_1199.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz SEO_1017.1.fq.gz
mv sample_CAACCG.2.fq.gz SEO_1017.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz SEO_1017.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz SEO_1017.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz SEO_0964P01.1.fq.gz
mv sample_CATCAA.2.fq.gz SEO_0964P01.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz SEO_0964P01.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz SEO_0964P01.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz NOO_1165.1.fq.gz
mv sample_CCAGCT.2.fq.gz NOO_1165.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz NOO_1165.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz NOO_1165.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz SWO_1229P01.1.fq.gz
mv sample_CCGACC.2.fq.gz SWO_1229P01.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz SWO_1229P01.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz SWO_1229P01.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz SEO_0965.1.fq.gz
mv sample_CGAGGC.2.fq.gz SEO_0965.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz SEO_0965.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz SEO_0965.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz SEO_0961.1.fq.gz
mv sample_CTGGTT.2.fq.gz SEO_0961.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz SEO_0961.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz SEO_0961.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz SEO_0928.1.fq.gz
mv sample_GATTAC.2.fq.gz SEO_0928.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz SEO_0928.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz SEO_0928.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz SEO_1016.1.fq.gz
mv sample_GCCTGG.2.fq.gz SEO_1016.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz SEO_1016.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz SEO_1016.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz SEO_1200.1.fq.gz
mv sample_GGAGCG.2.fq.gz SEO_1200.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz SEO_1200.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz SEO_1200.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz NOO_1163.1.fq.gz
mv sample_GGTAGA.2.fq.gz NOO_1163.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz NOO_1163.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz NOO_1163.rem.2.fq.gz
mv sample_GGTCTT.1.fq.gz SEO_0853.1.fq.gz
mv sample_GGTCTT.2.fq.gz SEO_0853.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz SEO_0853.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz SEO_0853.rem.2.fq.gz
mv sample_GTATGA.1.fq.gz SEO_1048.1.fq.gz
mv sample_GTATGA.2.fq.gz SEO_1048.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz SEO_1048.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz SEO_1048.rem.2.fq.gz
mv sample_TCGCAT.1.fq.gz SEO_1080.1.fq.gz
mv sample_TCGCAT.2.fq.gz SEO_1080.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz SEO_1080.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz SEO_1080.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz SEO_1045.1.fq.gz
mv sample_TGCTTG.2.fq.gz SEO_1045.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz SEO_1045.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz SEO_1045.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz SEO_1085.1.fq.gz
mv sample_TGGATT.2.fq.gz SEO_1085.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz SEO_1085.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz SEO_1085.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz SEO_1079.1.fq.gz
mv sample_TTACGG.2.fq.gz SEO_1079.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz SEO_1079.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz SEO_1079.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz SWO_1231.1.fq.gz
mv sample_TTGAAC.2.fq.gz SWO_1231.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz SWO_1231.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz SWO_1231.rem.2.fq.gz

mv process_radtags.log Pool1_SEH_AATCCG_S3_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_02
module load stacks

process_radtags -1 Pool1_SEH_ACTGGT_S4_L001_R1_001.fastq.gz -2 Pool1_SEH_ACTGGT_S4_L001_R2_001.fastq.gz -o ./Pool1_SEH_ACTGGT_S4_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool1_SEH_ACTGGT_S4_samples/

mv sample_AACGAA.1.fq.gz NOO_1168.1.fq.gz
mv sample_AACGAA.2.fq.gz NOO_1168.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz NOO_1168.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz NOO_1168.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz SWO_1210.1.fq.gz
mv sample_AACTCG.2.fq.gz SWO_1210.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz SWO_1210.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz SWO_1210.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz SEO_1019.1.fq.gz
mv sample_AAGATA.2.fq.gz SEO_1019.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz SEO_1019.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz SEO_1019.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz WTD_P01_Neg.1.fq.gz
mv sample_ACCAGA.2.fq.gz WTD_P01_Neg.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz WTD_P01_Neg.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz WTD_P01_Neg.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz SEO_1050.1.fq.gz
mv sample_ACTTCC.2.fq.gz SEO_1050.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz SEO_1050.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz SEO_1050.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz NOO_0932.1.fq.gz
mv sample_ATCGTC.2.fq.gz NOO_0932.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz NOO_0932.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz NOO_0932.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz SWO_1205.1.fq.gz
mv sample_ATTCAT.2.fq.gz SWO_1205.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz SWO_1205.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz SWO_1205.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz NOO_1025P01.1.fq.gz
mv sample_CAACCG.2.fq.gz NOO_1025P01.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz NOO_1025P01.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz NOO_1025P01.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz SEO_0970.1.fq.gz
mv sample_CATCAA.2.fq.gz SEO_0970.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz SEO_0970.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz SEO_0970.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz NOO_1175.1.fq.gz
mv sample_CCAGCT.2.fq.gz NOO_1175.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz NOO_1175.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz NOO_1175.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz NOO_1235.1.fq.gz
mv sample_CCGACC.2.fq.gz NOO_1235.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz NOO_1235.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz NOO_1235.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz SWO_0987.1.fq.gz
mv sample_CGAGGC.2.fq.gz SWO_0987.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz SWO_0987.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz SWO_0987.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz SEO_0969.1.fq.gz
mv sample_CTGGTT.2.fq.gz SEO_0969.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz SEO_0969.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz SEO_0969.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz NOO_0933.1.fq.gz
mv sample_GATTAC.2.fq.gz NOO_0933.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz NOO_0933.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz NOO_0933.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz SEO_1020.1.fq.gz
mv sample_GCCTGG.2.fq.gz SEO_1020.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz SEO_1020.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz SEO_1020.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz SWO_1209.1.fq.gz
mv sample_GGAGCG.2.fq.gz SWO_1209.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz SWO_1209.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz SWO_1209.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz NOO_1174.1.fq.gz
mv sample_GGTAGA.2.fq.gz NOO_1174.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz NOO_1174.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz NOO_1174.rem.2.fq.gz
mv sample_GGTCTT.1.fq.gz NOO_0930.1.fq.gz
mv sample_GGTCTT.2.fq.gz NOO_0930.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz NOO_0930.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz NOO_0930.rem.2.fq.gz
mv sample_GTATGA.1.fq.gz SEO_1056.1.fq.gz
mv sample_GTATGA.2.fq.gz SEO_1056.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz SEO_1056.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz SEO_1056.rem.2.fq.gz
mv sample_TCGCAT.1.fq.gz SEO_1089.1.fq.gz
mv sample_TCGCAT.2.fq.gz SEO_1089.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz SEO_1089.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz SEO_1089.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz SEO_1053.1.fq.gz
mv sample_TGCTTG.2.fq.gz SEO_1053.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz SEO_1053.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz SEO_1053.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz SEO_1090.1.fq.gz
mv sample_TGGATT.2.fq.gz SEO_1090.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz SEO_1090.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz SEO_1090.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz SEO_1087.1.fq.gz
mv sample_TTACGG.2.fq.gz SEO_1087.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz SEO_1087.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz SEO_1087.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz NOO_1239.1.fq.gz
mv sample_TTGAAC.2.fq.gz NOO_1239.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz NOO_1239.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz NOO_1239.rem.2.fq.gz

mv process_radtags.log Pool1_SEH_ACTGGT_S4_process_radtags.log

cd .. 
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_03
module load stacks

process_radtags -1 Pool1_SEH_ATCGAA_S8_L001_R1_001.fastq.gz -2 Pool1_SEH_ATCGAA_S8_L001_R2_001.fastq.gz -o ./Pool1_SEH_ATCGAA_S8_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool1_SEH_ATCGAA_S8_samples/

mv sample_AACGAA.1.fq.gz SWO_1472.1.fq.gz
mv sample_AACGAA.2.fq.gz SWO_1472.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz SWO_1472.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz SWO_1472.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz SEO_1507.1.fq.gz
mv sample_AACTCG.2.fq.gz SEO_1507.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz SEO_1507.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz SEO_1507.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz SEO_1351.1.fq.gz
mv sample_AAGATA.2.fq.gz SEO_1351.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz SEO_1351.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz SEO_1351.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz WTD_P02_neg.1.fq.gz
mv sample_ACCAGA.2.fq.gz WTD_P02_neg.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz WTD_P02_neg.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz WTD_P02_neg.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz SWO_1390.1.fq.gz
mv sample_ACTTCC.2.fq.gz SWO_1390.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz SWO_1390.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz SWO_1390.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz NOO_1276.1.fq.gz
mv sample_ATCGTC.2.fq.gz NOO_1276.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz NOO_1276.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz NOO_1276.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz SEO_1499.1.fq.gz
mv sample_ATTCAT.2.fq.gz SEO_1499.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz SEO_1499.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz SEO_1499.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz SEO_1357.1.fq.gz
mv sample_CAACCG.2.fq.gz SEO_1357.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz SEO_1357.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz SEO_1357.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz NOO_1323.1.fq.gz
mv sample_CATCAA.2.fq.gz NOO_1323.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz NOO_1323.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz NOO_1323.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz SWO_1477.1.fq.gz
mv sample_CCAGCT.2.fq.gz SWO_1477.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz SWO_1477.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz SWO_1477.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz NOO_1530.1.fq.gz
mv sample_CCGACC.2.fq.gz NOO_1530.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz NOO_1530.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz NOO_1530.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz NOO_1324.1.fq.gz
mv sample_CGAGGC.2.fq.gz NOO_1324.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz NOO_1324.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz NOO_1324.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz NOO_1320.1.fq.gz
mv sample_CTGGTT.2.fq.gz NOO_1320.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz NOO_1320.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz NOO_1320.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz NOO_1277.1.fq.gz
mv sample_GATTAC.2.fq.gz NOO_1277.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz NOO_1277.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz NOO_1277.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz SEO_1356.1.fq.gz
mv sample_GCCTGG.2.fq.gz SEO_1356.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz SEO_1356.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz SEO_1356.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz SEO_1503.1.fq.gz
mv sample_GGAGCG.2.fq.gz SEO_1503.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz SEO_1503.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz SEO_1503.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz SWO_1473P02.1.fq.gz
mv sample_GGTAGA.2.fq.gz SWO_1473P02.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz SWO_1473P02.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz SWO_1473P02.rem.2.fq.gz
mv sample_GGTCTT.1.fq.gz NOO_1275.1.fq.gz
mv sample_GGTCTT.2.fq.gz NOO_1275.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz NOO_1275.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz NOO_1275.rem.2.fq.gz
mv sample_GTATGA.1.fq.gz SWO_1395.1.fq.gz
mv sample_GTATGA.2.fq.gz SWO_1395.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz SWO_1395.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz SWO_1395.rem.2.fq.gz
mv sample_TCGCAT.1.fq.gz SWO_1441.1.fq.gz
mv sample_TCGCAT.2.fq.gz SWO_1441.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz SWO_1441.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz SWO_1441.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz SWO_1394.1.fq.gz
mv sample_TGCTTG.2.fq.gz SWO_1394.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz SWO_1394.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz SWO_1394.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz SWO_1442.1.fq.gz
mv sample_TGGATT.2.fq.gz SWO_1442.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz SWO_1442.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz SWO_1442.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz SWO_1435.1.fq.gz
mv sample_TTACGG.2.fq.gz SWO_1435.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz SWO_1435.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz SWO_1435.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz NOO_1533.1.fq.gz
mv sample_TTGAAC.2.fq.gz NOO_1533.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz NOO_1533.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz NOO_1533.rem.2.fq.gz

mv process_radtags.log Pool1_SEH_ATCGAA_S8_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_04
module load stacks

process_radtags -1 Pool1_SEH_CGGTTA_S6_L001_R1_001.fastq.gz -2 Pool1_SEH_CGGTTA_S6_L001_R2_001.fastq.gz -o ./Pool1_SEH_CGGTTA_S6_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool1_SEH_CGGTTA_S6_samples/

mv sample_GGTCTT.1.fq.gz NOO_1262.1.fq.gz
mv sample_GGTCTT.2.fq.gz NOO_1262.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz NOO_1262.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz NOO_1262.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz NOO_1264.1.fq.gz
mv sample_ATCGTC.2.fq.gz NOO_1264.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz NOO_1264.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz NOO_1264.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz NOO_1265.1.fq.gz
mv sample_GATTAC.2.fq.gz NOO_1265.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz NOO_1265.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz NOO_1265.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz NOO_1290.1.fq.gz
mv sample_CTGGTT.2.fq.gz NOO_1290.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz NOO_1290.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz NOO_1290.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz NOO_1306.1.fq.gz
mv sample_CATCAA.2.fq.gz NOO_1306.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz NOO_1306.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz NOO_1306.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz NOO_1307.1.fq.gz
mv sample_CGAGGC.2.fq.gz NOO_1307.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz NOO_1307.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz NOO_1307.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz SEO_1337.1.fq.gz
mv sample_AAGATA.2.fq.gz SEO_1337.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz SEO_1337.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz SEO_1337.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz SEO_1339.1.fq.gz
mv sample_GCCTGG.2.fq.gz SEO_1339.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz SEO_1339.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz SEO_1339.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz SEO_1340.1.fq.gz
mv sample_CAACCG.2.fq.gz SEO_1340.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz SEO_1340.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz SEO_1340.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz SEO_1363.1.fq.gz
mv sample_ACTTCC.2.fq.gz SEO_1363.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz SEO_1363.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz SEO_1363.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz SEO_1368.1.fq.gz
mv sample_TGCTTG.2.fq.gz SEO_1368.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz SEO_1368.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz SEO_1368.rem.2.fq.gz
mv sample_GTATGA.1.fq.gz 1369.1.fq.gz
mv sample_GTATGA.2.fq.gz SEO_1369.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz SEO_1369.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz SEO_1369.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz SEO_1415.1.fq.gz
mv sample_TTACGG.2.fq.gz SEO_1415.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz SEO_1415.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz SEO_1415.rem.2.fq.gz
mv sample_TCGCAT.1.fq.gz SWO_1426.1.fq.gz
mv sample_TCGCAT.2.fq.gz SWO_1426.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz SWO_1426.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz SWO_1426.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz SWO_1427.1.fq.gz
mv sample_TGGATT.2.fq.gz SWO_1427.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz SWO_1427.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz SWO_1427.rem.2.fq.gz
mv sample_AACGAA.1.fq.gz SWO_1450.1.fq.gz
mv sample_AACGAA.2.fq.gz SWO_1450.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz SWO_1450.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz SWO_1450.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz SWO_1453.1.fq.gz
mv sample_GGTAGA.2.fq.gz SWO_1453.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz SWO_1453.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz SWO_1453.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz SWO_1454.1.fq.gz
mv sample_CCAGCT.2.fq.gz SWO_1454.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz SWO_1454.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz SWO_1454.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz SEO_1484.1.fq.gz
mv sample_ATTCAT.2.fq.gz SEO_1484.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz SEO_1484.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz SEO_1484.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz SEO_1491.1.fq.gz
mv sample_GGAGCG.2.fq.gz SEO_1491.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz SEO_1491.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz SEO_1491.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz SEO_1492.1.fq.gz
mv sample_AACTCG.2.fq.gz SEO_1492.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz SEO_1492.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz SEO_1492.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz SEO_1514.1.fq.gz
mv sample_CCGACC.2.fq.gz SEO_1514.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz SEO_1514.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz SEO_1514.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz SEO_1521_P02.1.fq.gz
mv sample_TTGAAC.2.fq.gz SEO_1521_P02.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz SEO_1521_P02.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz SEO_1521_P02.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz SEO_1522_P02.1.fq.gz
mv sample_ACCAGA.2.fq.gz SEO_1522_P02.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz SEO_1522_P02.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz SEO_1522_P02.rem.2.fq.gz

mv process_radtags.log Pool1_SEH_CGGTTA_S6_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_05
module load stacks

process_radtags -1 Pool1_SEH_GCTACA_S2_L001_R1_001.fastq.gz -2 Pool1_SEH_GCTACA_S2_L001_R2_001.fastq.gz -o ./Pool1_SEH_GCTACA_S2_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool1_SEH_GCTACA_S2_samples/

mv sample_GGTCTT.1.fq.gz SEO_0635.1.fq.gz
mv sample_GGTCTT.2.fq.gz SEO_0635.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz SEO_0635.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz SEO_0635.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz SEO_0710.1.fq.gz
mv sample_ATCGTC.2.fq.gz SEO_0710.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz SEO_0710.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz SEO_0710.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz SEO_0824.1.fq.gz
mv sample_GATTAC.2.fq.gz SEO_0824.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz SEO_0824.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz SEO_0824.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz SEO_0954.1.fq.gz
mv sample_CTGGTT.2.fq.gz SEO_0954.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz SEO_0954.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz SEO_0954.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz SEO_0955.1.fq.gz
mv sample_CATCAA.2.fq.gz SEO_0955.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz SEO_0955.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz SEO_0955.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz SEO_0960.1.fq.gz
mv sample_CGAGGC.2.fq.gz SEO_0960.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz SEO_0960.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz SEO_0960.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz NOO_0995.1.fq.gz
mv sample_AAGATA.2.fq.gz NOO_0995.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz NOO_0995.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz NOO_0995.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz NOO_0998.1.fq.gz
mv sample_GCCTGG.2.fq.gz NOO_0998.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz NOO_0998.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz NOO_0998.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz NOO_0999.1.fq.gz
mv sample_CAACCG.2.fq.gz NOO_0999.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz NOO_0999.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz NOO_0999.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz NOO_1035.1.fq.gz
mv sample_ACTTCC.2.fq.gz NOO_1035.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz NOO_1035.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz NOO_1035.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz NOO_1036.1.fq.gz
mv sample_TGCTTG.2.fq.gz NOO_1036.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz NOO_1036.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz NOO_1036.rem.2.fq.gz
mv sample_GTATGA.1.fq.gz NOO_1039.1.fq.gz
mv sample_GTATGA.2.fq.gz NOO_1039.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz NOO_1039.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz NOO_1039.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz SEO_1068.1.fq.gz
mv sample_TTACGG.2.fq.gz SEO_1068.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz SEO_1068.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz SEO_1068.rem.2.fq.gz
mv sample_TCGCAT.1.fq.gz SEO_1074.1.fq.gz
mv sample_TCGCAT.2.fq.gz SEO_1074.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz SEO_1074.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz SEO_1074.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz SEO_1075.1.fq.gz
mv sample_TGGATT.2.fq.gz SEO_1075.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz SEO_1075.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz SEO_1075.rem.2.fq.gz
mv sample_AACGAA.1.fq.gz SEO_1101.1.fq.gz
mv sample_AACGAA.2.fq.gz SEO_1101.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz SEO_1101.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz SEO_1101.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz NOO_1154.1.fq.gz
mv sample_GGTAGA.2.fq.gz NOO_1154.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz NOO_1154.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz NOO_1154.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz NOO_1155.1.fq.gz
mv sample_CCAGCT.2.fq.gz NOO_1155.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz NOO_1155.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz NOO_1155.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz SEO_1190.1.fq.gz
mv sample_ATTCAT.2.fq.gz SEO_1190.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz SEO_1190.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz SEO_1190.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz SEO_1194.1.fq.gz
mv sample_GGAGCG.2.fq.gz SEO_1194.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz SEO_1194.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz SEO_1194.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz SEO_1195.1.fq.gz
mv sample_AACTCG.2.fq.gz SEO_1195.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz SEO_1195.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz SEO_1195.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz NOO_1222.1.fq.gz
mv sample_CCGACC.2.fq.gz NOO_1222.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz NOO_1222.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz NOO_1222.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz SWO_1224_P01.1.fq.gz
mv sample_TTGAAC.2.fq.gz SWO_1224_P01.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz SWO_1224_P01.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz SWO_1224_P01.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz SWO_1225_P01.1.fq.gz
mv sample_ACCAGA.2.fq.gz SWO_1225_P01.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz SWO_1225_P01.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz SWO_1225_P01.rem.2.fq.gz


mv process_radtags.log Pool1_SEH_GCTACA_S2_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_06
module load stacks

process_radtags -1 Pool1_SEH_GTCTAG_S7_L001_R1_001.fastq.gz -2 Pool1_SEH_GTCTAG_S7_L001_R2_001.fastq.gz -o ./Pool1_SEH_GTCTAG_S7_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq


cd Pool1_SEH_GTCTAG_S7_samples/

mv sample_GGTCTT.1.fq.gz NOO_1271.1.fq.gz
mv sample_GGTCTT.2.fq.gz NOO_1271.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz NOO_1271.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz NOO_1271.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz NOO_1273.1.fq.gz
mv sample_ATCGTC.2.fq.gz NOO_1273.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz NOO_1273.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz NOO_1273.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz NOO_1274.1.fq.gz
mv sample_GATTAC.2.fq.gz NOO_1274.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz NOO_1274.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz NOO_1274.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz NOO_1310.1.fq.gz
mv sample_CTGGTT.2.fq.gz NOO_1310.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz NOO_1310.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz NOO_1310.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz NOO_1312.1.fq.gz
mv sample_CATCAA.2.fq.gz NOO_1312.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz NOO_1312.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz NOO_1312.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz NOO_1319.1.fq.gz
mv sample_CGAGGC.2.fq.gz NOO_1319.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz NOO_1319.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz NOO_1319.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz SEO_1345.1.fq.gz
mv sample_AAGATA.2.fq.gz SEO_1345.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz SEO_1345.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz SEO_1345.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz SEO_1346.1.fq.gz
mv sample_GCCTGG.2.fq.gz SEO_1346.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz SEO_1346.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz SEO_1346.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz SEO_1350.1.fq.gz
mv sample_CAACCG.2.fq.gz SEO_1350.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz SEO_1350.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz SEO_1350.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz SEO_1374.1.fq.gz
mv sample_ACTTCC.2.fq.gz SEO_1374.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz SEO_1374.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz SEO_1374.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz SEO_1375.1.fq.gz
mv sample_TGCTTG.2.fq.gz SEO_1375.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz SEO_1375.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz SEO_1375.rem.2.fq.gz
mv sample_GTATGA.1.fq.gz SWO_1389.1.fq.gz
mv sample_GTATGA.2.fq.gz SWO_1389.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz SWO_1389.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz SWO_1389.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz SWO_1429.1.fq.gz
mv sample_TTACGG.2.fq.gz SWO_1429.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz SWO_1429.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz SWO_1429.rem.2.fq.gz
mv sample_TCGCAT.1.fq.gz SWO_1432.1.fq.gz
mv sample_TCGCAT.2.fq.gz SWO_1432.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz SWO_1432.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz SWO_1432.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz SWO_1433.1.fq.gz
mv sample_TGGATT.2.fq.gz SWO_1433.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz SWO_1433.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz SWO_1433.rem.2.fq.gz
mv sample_AACGAA.1.fq.gz SEO_1458.1.fq.gz
mv sample_AACGAA.2.fq.gz SEO_1458.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz SEO_1458.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz SEO_1458.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz SEO_1466.1.fq.gz
mv sample_GGTAGA.2.fq.gz SEO_1466.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz SEO_1466.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz SEO_1466.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz SWO_1470.1.fq.gz
mv sample_CCAGCT.2.fq.gz SWO_1470.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz SWO_1470.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz SWO_1470.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz SEO_1494.1.fq.gz
mv sample_ATTCAT.2.fq.gz SEO_1494.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz SEO_1494.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz SEO_1494.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz SEO_1496.1.fq.gz
mv sample_GGAGCG.2.fq.gz SEO_1496.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz SEO_1496.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz SEO_1496.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz SEO_1498.1.fq.gz
mv sample_AACTCG.2.fq.gz SEO_1498.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz SEO_1498.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz SEO_1498.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz SEO_1525_P02.1.fq.gz
mv sample_CCGACC.2.fq.gz SEO_1525_P02.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz SEO_1525_P02.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz SEO_1525_P02.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz SEO_1526.1.fq.gz
mv sample_TTGAAC.2.fq.gz SEO_1526.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz SEO_1526.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz SEO_1526.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz NOO_1529.1.fq.gz
mv sample_ACCAGA.2.fq.gz NOO_1529.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz NOO_1529.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz NOO_1529.rem.2.fq.gz

mv process_radtags.log Pool1_SEH_GTCTAG_S7_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_07
module load stacks

process_radtags -1 Pool1_SEH_TAGTGC_S1_L001_R1_001.fastq.gz -2 Pool1_SEH_TAGTGC_S1_L001_R2_001.fastq.gz -o ./Pool1_SEH_TAGTGC_S1_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool1_SEH_TAGTGC_S1_samples/

mv sample_CTGGTT.1.fq.gz SEO_0630.1.fq.gz
mv sample_CTGGTT.2.fq.gz SEO_0630.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz SEO_0630.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz SEO_0630.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz SEO_0631.1.fq.gz
mv sample_ATCGTC.2.fq.gz SEO_0631.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz SEO_0631.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz SEO_0631.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz SEO_0632.1.fq.gz
mv sample_GATTAC.2.fq.gz SEO_0632.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz SEO_0632.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz SEO_0632.rem.2.fq.gz
mv sample_GGTCTT.1.fq.gz NOO_0934_P01.1.fq.gz
mv sample_GGTCTT.2.fq.gz NOO_0934_P01.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz NOO_0934_P01.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz NOO_0934_P01.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz NOO_0940.1.fq.gz
mv sample_CATCAA.2.fq.gz NOO_0940.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz NOO_0940.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz NOO_0940.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz NOO_0942.1.fq.gz
mv sample_CGAGGC.2.fq.gz NOO_0942.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz NOO_0942.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz NOO_0942.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz SWO_0988.1.fq.gz
mv sample_AAGATA.2.fq.gz SWO_0988.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz SWO_0988.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz SWO_0988.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz SWO_0989.1.fq.gz
mv sample_GCCTGG.2.fq.gz SWO_0989.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz SWO_0989.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz SWO_0989.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz SWO_0991.1.fq.gz
mv sample_CAACCG.2.fq.gz SWO_0991.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz SWO_0991.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz SWO_0991.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz NOO_1028_P01.1.fq.gz
mv sample_ACTTCC.2.fq.gz NOO_1028_P01.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz NOO_1028_P01.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz NOO_1028_P01.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz NOO_1029_P01.1.fq.gz
mv sample_TGCTTG.2.fq.gz NOO_1029_P01.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz NOO_1029_P01.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz NOO_1029_P01.rem.2.fq.gz
mv sample_GTATGA.1.fq.gz NOO_1030_P01.1.fq.gz
mv sample_GTATGA.2.fq.gz NOO_1030_P01.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz NOO_1030_P01.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz NOO_1030_P01.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz SEO_1058_P01.1.fq.gz
mv sample_TTACGG.2.fq.gz SEO_1058_P01.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz SEO_1058_P01.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz SEO_1058_P01.rem.2.fq.gz
mv sample_TCGCAT.1.fq.gz SEO_1063_P01.1.fq.gz
mv sample_TCGCAT.2.fq.gz SEO_1063_P01.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz SEO_1063_P01.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz SEO_1063_P01.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz SEO_1065.1.fq.gz
mv sample_TGGATT.2.fq.gz SEO_1065.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz SEO_1065.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz SEO_1065.rem.2.fq.gz
mv sample_AACGAA.1.fq.gz SEO_1096.1.fq.gz
mv sample_AACGAA.2.fq.gz SEO_1096.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz SEO_1096.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz SEO_1096.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz SEO_1098.1.fq.gz
mv sample_GGTAGA.2.fq.gz SEO_1098.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz SEO_1098.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz SEO_1098.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz SEO_1100_P01.1.fq.gz
mv sample_CCAGCT.2.fq.gz SEO_1100_P01.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz SEO_1100_P01.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz SEO_1100_P01.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz SEO_1185.1.fq.gz
mv sample_ATTCAT.2.fq.gz SEO_1185.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz SEO_1185.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz SEO_1185.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz SEO_1186_P01.1.fq.gz
mv sample_GGAGCG.2.fq.gz SEO_1186_P01.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz SEO_1186_P01.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz SEO_1186_P01.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz SEO_1189P01.1.fq.gz
mv sample_AACTCG.2.fq.gz SEO_1189P01.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz SEO_1189P01.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz SEO_1189P01.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz NOO_1214.1.fq.gz
mv sample_CCGACC.2.fq.gz NOO_1214.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz NOO_1214.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz NOO_1214.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz NOO_1216.1.fq.gz
mv sample_TTGAAC.2.fq.gz NOO_1216.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz NOO_1216.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz NOO_1216.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz NOO_1220.1.fq.gz
mv sample_ACCAGA.2.fq.gz NOO_1220.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz NOO_1220.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz NOO_1220.rem.2.fq.gz

mv process_radtags.log Pool1_SEH_TAGTGC_S1_rocess_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_08
module load stacks

process_radtags -1 Pool1_SEH_TGTCAC_S5_L001_R1_001.fastq.gz -2 Pool1_SEH_TGTCAC_S5_L001_R2_001.fastq.gz -o ./Pool1_SEH_TGTCAC_S5_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool1_SEH_TGTCAC_S5_samples/

mv sample_GGTCTT.1.fq.gz NOO_1241.1.fq.gz
mv sample_GGTCTT.2.fq.gz NOO_1241.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz NOO_1241.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz NOO_1241.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz NOO_1258.1.fq.gz
mv sample_ATCGTC.2.fq.gz NOO_1258.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz NOO_1258.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz NOO_1258.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz NOO_1259.1.fq.gz
mv sample_GATTAC.2.fq.gz NOO_1259.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz NOO_1259.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz NOO_1259.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz NOO_1284.1.fq.gz
mv sample_CTGGTT.2.fq.gz NOO_1284.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz NOO_1284.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz NOO_1284.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz NOO_1288.1.fq.gz
mv sample_CATCAA.2.fq.gz NOO_1288.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz NOO_1288.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz NOO_1288.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz NOO_1289.1.fq.gz
mv sample_CGAGGC.2.fq.gz NOO_1289.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz NOO_1289.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz NOO_1289.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz SEO_1328.1.fq.gz
mv sample_AAGATA.2.fq.gz SEO_1328.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz SEO_1328.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz SEO_1328.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz SEO_1330.1.fq.gz
mv sample_GCCTGG.2.fq.gz SEO_1330.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz SEO_1330.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz SEO_1330.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz SEO_1336.1.fq.gz
mv sample_CAACCG.2.fq.gz SEO_1336.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz SEO_1336.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz SEO_1336.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz SEO_1360.1.fq.gz
mv sample_ACTTCC.2.fq.gz SEO_1360.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz SEO_1360.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz SEO_1360.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz SEO_1361.1.fq.gz
mv sample_TGCTTG.2.fq.gz SEO_1361.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz SEO_1361.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz SEO_1361.rem.2.fq.gz
mv sample_GTATGA.1.fq.gz SEO_1362.1.fq.gz
mv sample_GTATGA.2.fq.gz SEO_1362.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz SEO_1362.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz SEO_1362.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz SEO_1409.1.fq.gz
mv sample_TTACGG.2.fq.gz SEO_1409.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz SEO_1409.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz SEO_1409.rem.2.fq.gz
mv sample_TCGCAT.1.fq.gz SEO_1412.1.fq.gz
mv sample_TCGCAT.2.fq.gz SEO_1412.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz SEO_1412.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz SEO_1412.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz SEO_1414.1.fq.gz
mv sample_TGGATT.2.fq.gz SEO_1414.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz SEO_1414.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz SEO_1414.rem.2.fq.gz
mv sample_AACGAA.1.fq.gz SWO_1443.1.fq.gz
mv sample_AACGAA.2.fq.gz SWO_1443.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz SWO_1443.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz SWO_1443.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz SWO_1444.1.fq.gz
mv sample_GGTAGA.2.fq.gz SWO_1444.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz SWO_1444.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz SWO_1444.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz SWO_1449.1.fq.gz
mv sample_CCAGCT.2.fq.gz SWO_1449.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz SWO_1449.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz SWO_1449.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz SEO_1478.1.fq.gz
mv sample_ATTCAT.2.fq.gz SEO_1478.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz SEO_1478.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz SEO_1478.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz SEO_1481.1.fq.gz
mv sample_GGAGCG.2.fq.gz SEO_1481.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz SEO_1481.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz SEO_1481.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz SEO_1483.1.fq.gz
mv sample_AACTCG.2.fq.gz SEO_1483.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz SEO_1483.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz SEO_1483.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz SEO_1508.1.fq.gz
mv sample_CCGACC.2.fq.gz SEO_1508.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz SEO_1508.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz SEO_1508.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz SEO_1512.1.fq.gz
mv sample_TTGAAC.2.fq.gz SEO_1512.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz SEO_1512.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz SEO_1512.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz SEO_1513.1.fq.gz
mv sample_ACCAGA.2.fq.gz SEO_1513.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz SEO_1513.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz SEO_1513.rem.2.fq.gz

mv process_radtags.log Pool1_SEH_TGTCAC_S5_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_09
module load stacks

process_radtags -1 Pool2_SEH_EW_AATCCG_S11_L002_R1_001.fastq.gz -2 Pool2_SEH_EW_AATCCG_S11_L002_R2_001.fastq.gz -o ./Pool2_SEH_EW_AATCCG_S11_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool2_SEH_EW_AATCCG_S11_samples/

mv sample_GTATGA.1.fq.gz HEMI_3304_GTATGA.1.fq.gz
mv sample_GTATGA.2.fq.gz HEMI_3304_GTATGA.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz HEMI_3304_GTATGA.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz HEMI_3304_GTATGA.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz HEMI_361_TTACGG.1.fq.gz
mv sample_TTACGG.2.fq.gz HEMI_361_TTACGG.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz HEMI_361_TTACGG.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz HEMI_361_TTACGG.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz HEMI_364_TGGATT.1.fq.gz
mv sample_TGGATT.2.fq.gz HEMI_364_TGGATT.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz HEMI_364_TGGATT.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz HEMI_364_TGGATT.rem.2.fq.gz
mv sample_TCGCAT.1.fq.gz HEMI_368_TCGCAT.1.fq.gz
mv sample_TCGCAT.2.fq.gz HEMI_368_TCGCAT.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz HEMI_368_TCGCAT.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz HEMI_368_TCGCAT.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz HEMI_373_TGCTTG.1.fq.gz
mv sample_TGCTTG.2.fq.gz HEMI_373_TGCTTG.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz HEMI_373_TGCTTG.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz HEMI_373_TGCTTG.rem.2.fq.gz
mv sample_AACGAA.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_AACGAA.1.fq.gz
mv sample_AACGAA.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_AACGAA.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_AACGAA.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_AACGAA.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_AACTCG.1.fq.gz
mv sample_AACTCG.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_AACTCG.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_AACTCG.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_AACTCG.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_ACCAGA.1.fq.gz
mv sample_ACCAGA.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_ACCAGA.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_ACCAGA.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_ACCAGA.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_ATTCAT.1.fq.gz
mv sample_ATTCAT.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_ATTCAT.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_ATTCAT.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_ATTCAT.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_CCAGCT.1.fq.gz
mv sample_CCAGCT.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_CCAGCT.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_CCAGCT.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_CCAGCT.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_CCGACC.1.fq.gz
mv sample_CCGACC.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_CCGACC.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_CCGACC.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_CCGACC.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_GGAGCG.1.fq.gz
mv sample_GGAGCG.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_GGAGCG.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_GGAGCG.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_GGAGCG.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_GGTAGA.1.fq.gz
mv sample_GGTAGA.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_GGTAGA.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_GGTAGA.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_GGTAGA.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_TTGAAC.1.fq.gz
mv sample_TTGAAC.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_TTGAAC.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_TTGAAC.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz Pool2_SEH_EW_AATCCG_S11_L002_TTGAAC.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz NOO_1030.1.fq.gz
mv sample_AAGATA.2.fq.gz NOO_1030.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz NOO_1030.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz NOO_1030.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz SEO_1058.1.fq.gz
mv sample_GCCTGG.2.fq.gz SEO_1058.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz SEO_1058.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz SEO_1058.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz SEO_1063.1.fq.gz
mv sample_CAACCG.2.fq.gz SEO_1063.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz SEO_1063.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz SEO_1063.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz SEO_1525.1.fq.gz
mv sample_ACTTCC.2.fq.gz SEO_1525.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz SEO_1525.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz SEO_1525.rem.2.fq.gz
mv sample_GGTCTT.1.fq.gz SEO_1594.1.fq.gz
mv sample_GGTCTT.2.fq.gz SEO_1594.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz SEO_1594.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz SEO_1594.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz SEO_1598.1.fq.gz
mv sample_ATCGTC.2.fq.gz SEO_1598.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz SEO_1598.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz SEO_1598.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz SEO_1599.1.fq.gz
mv sample_GATTAC.2.fq.gz SEO_1599.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz SEO_1599.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz SEO_1599.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz SWO_1635.1.fq.gz
mv sample_CTGGTT.2.fq.gz SWO_1635.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz SWO_1635.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz SWO_1635.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz SWO_1639.1.fq.gz
mv sample_CATCAA.2.fq.gz SWO_1639.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz SWO_1639.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz SWO_1639.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz SWO_1640.1.fq.gz
mv sample_CGAGGC.2.fq.gz SWO_1640.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz SWO_1640.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz SWO_1640.rem.2.fq.gz

mv process_radtags.log Pool2_SEH_EW_AATCCG_S11_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_10
module load stacks

process_radtags -1 Pool2_SEH_EW_ACTGGT_S12_L002_R1_001.fastq.gz -2 Pool2_SEH_EW_ACTGGT_S12_L002_R2_001.fastq.gz -o ./Pool2_SEH_EW_ACTGGT_S12_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool2_SEH_EW_ACTGGT_S12_samples/

mv sample_GTATGA.1.fq.gz HEMI_3296_GTATGA.1.fq.gz
mv sample_GTATGA.2.fq.gz HEMI_3296_GTATGA.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz HEMI_3296_GTATGA.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz HEMI_3296_GTATGA.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz HEMI_360_TGCTTG.1.fq.gz
mv sample_TGCTTG.2.fq.gz HEMI_360_TGCTTG.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz HEMI_360_TGCTTG.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz HEMI_360_TGCTTG.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz HEMI_362_TTACGG.1.fq.gz
mv sample_TTACGG.2.fq.gz HEMI_362_TTACGG.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz HEMI_362_TTACGG.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz HEMI_362_TTACGG.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz HEMI_372_ACTTCC.1.fq.gz
mv sample_ACTTCC.2.fq.gz HEMI_372_ACTTCC.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz HEMI_372_ACTTCC.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz HEMI_372_ACTTCC.rem.2.fq.gz
mv sample_TCGCAT.1.fq.gz HEMI_910_TCGCAT.1.fq.gz
mv sample_TCGCAT.2.fq.gz HEMI_910_TCGCAT.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz HEMI_910_TCGCAT.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz HEMI_910_TCGCAT.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz HEMI_R_398_TGGATT.1.fq.gz
mv sample_TGGATT.2.fq.gz HEMI_R_398_TGGATT.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz HEMI_R_398_TGGATT.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz HEMI_R_398_TGGATT.rem.2.fq.gz
mv sample_AACGAA.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_AACGAA.1.fq.gz
mv sample_AACGAA.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_AACGAA.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_AACGAA.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_AACGAA.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_AACTCG.1.fq.gz
mv sample_AACTCG.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_AACTCG.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_AACTCG.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_AACTCG.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_ACCAGA.1.fq.gz
mv sample_ACCAGA.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_ACCAGA.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_ACCAGA.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_ACCAGA.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_ATTCAT.1.fq.gz
mv sample_ATTCAT.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_ATTCAT.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_ATTCAT.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_ATTCAT.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_CCAGCT.1.fq.gz
mv sample_CCAGCT.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_CCAGCT.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_CCAGCT.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_CCAGCT.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_CCGACC.1.fq.gz
mv sample_CCGACC.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_CCGACC.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_CCGACC.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_CCGACC.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_GGAGCG.1.fq.gz
mv sample_GGAGCG.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_GGAGCG.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_GGAGCG.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_GGAGCG.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_GGTAGA.1.fq.gz
mv sample_GGTAGA.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_GGTAGA.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_GGTAGA.rem.1.fq.gz
mv sample_TTGAAC.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_TTGAAC.1.fq.gz
mv sample_TTGAAC.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_TTGAAC.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_TTGAAC.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_TTGAAC.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz SEO_1100.1.fq.gz
mv sample_AAGATA.2.fq.gz SEO_1100.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz SEO_1100.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz SEO_1100.rem.2.fq.gz
mv sample_GGTAGA.rem.2.fq.gz Pool2_SEH_EW_ACTGGT_S12_L002_GGTAGA.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz SEO_1186.1.fq.gz
mv sample_GCCTGG.2.fq.gz SEO_1186.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz SEO_1186.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz SEO_1186.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz SEO_1189.1.fq.gz
mv sample_CAACCG.2.fq.gz SEO_1189.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz SEO_1189.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz SEO_1189.rem.2.fq.gz
mv sample_GGTCTT.1.fq.gz SEO_1604.1.fq.gz
mv sample_GGTCTT.2.fq.gz SEO_1604.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz SEO_1604.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz SEO_1604.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz SEO_1605.1.fq.gz
mv sample_ATCGTC.2.fq.gz SEO_1605.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz SEO_1605.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz SEO_1605.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz SWO_1617.1.fq.gz
mv sample_GATTAC.2.fq.gz SWO_1617.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz SWO_1617.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz SWO_1617.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz SEO_1685.1.fq.gz
mv sample_CTGGTT.2.fq.gz SEO_1685.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz SEO_1685.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz SEO_1685.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz SEO_1693.1.fq.gz
mv sample_CATCAA.2.fq.gz SEO_1693.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz SEO_1693.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz SEO_1693.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz SEO_1695.1.fq.gz
mv sample_CGAGGC.2.fq.gz SEO_1695.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz SEO_1695.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz SEO_1695.rem.2.fq.gz

## Rename the LOG file 
mv process_radtags.log Pool2_SEH_EW_ACTGGT_S12process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_11
module load stacks

process_radtags -1 Pool2_SEH_EW_ATCGAA_S16_L002_R1_001.fastq.gz -2 Pool2_SEH_EW_ATCGAA_S16_L002_R2_001.fastq.gz -o ./Pool2_SEH_EW_ATCGAA_S16_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool2_SEH_EW_ATCGAA_S16_samples/

mv sample_AACGAA.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_AACGAA.1.fq.gz
mv sample_CATCAA.1.fq.gz HEMI_LT_3079_CATCAA.1.fq.gz
mv sample_GGTAGA.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GGTAGA.1.fq.gz
mv sample_AACGAA.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_AACGAA.2.fq.gz
mv sample_CATCAA.2.fq.gz HEMI_LT_3079_CATCAA.2.fq.gz
mv sample_GGTAGA.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GGTAGA.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_AACGAA.rem.1.fq.gz
mv sample_CATCAA.rem.1.fq.gz HEMI_LT_3079_CATCAA.rem.1.fq.gz
mv sample_GGTAGA.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GGTAGA.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_AACGAA.rem.2.fq.gz
mv sample_CATCAA.rem.2.fq.gz HEMI_LT_3079_CATCAA.rem.2.fq.gz
mv sample_GGTAGA.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GGTAGA.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_AACTCG.1.fq.gz
mv sample_CCAGCT.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_CCAGCT.1.fq.gz
mv sample_GGTCTT.1.fq.gz HEMI_LT_3095_GGTCTT.1.fq.gz
mv sample_AACTCG.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_AACTCG.2.fq.gz
mv sample_CCAGCT.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_CCAGCT.2.fq.gz
mv sample_GGTCTT.2.fq.gz HEMI_LT_3095_GGTCTT.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_AACTCG.rem.1.fq.gz
mv sample_CCAGCT.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_CCAGCT.rem.1.fq.gz
mv sample_GGTCTT.rem.1.fq.gz HEMI_LT_3095_GGTCTT.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_AACTCG.rem.2.fq.gz
mv sample_CCAGCT.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_CCAGCT.rem.2.fq.gz
mv sample_GGTCTT.rem.2.fq.gz HEMI_LT_3095_GGTCTT.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz HEMI_LT_3093_AAGATA.1.fq.gz
mv sample_CCGACC.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_CCGACC.1.fq.gz
mv sample_GTATGA.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GTATGA.1.fq.gz
mv sample_AAGATA.2.fq.gz HEMI_LT_3093_AAGATA.2.fq.gz
mv sample_CCGACC.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_CCGACC.2.fq.gz
mv sample_GTATGA.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GTATGA.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz HEMI_LT_3093_AAGATA.rem.1.fq.gz
mv sample_CCGACC.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_CCGACC.rem.1.fq.gz
mv sample_GTATGA.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GTATGA.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz HEMI_LT_3093_AAGATA.rem.2.fq.gz
mv sample_CCGACC.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_CCGACC.rem.2.fq.gz
mv sample_GTATGA.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GTATGA.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ACCAGA.1.fq.gz
mv sample_CGAGGC.1.fq.gz HEMI_LT_3080_CGAGGC.1.fq.gz
mv sample_TCGCAT.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TCGCAT.1.fq.gz
mv sample_ACCAGA.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ACCAGA.2.fq.gz
mv sample_CGAGGC.2.fq.gz HEMI_LT_3080_CGAGGC.2.fq.gz
mv sample_TCGCAT.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TCGCAT.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ACCAGA.rem.1.fq.gz
mv sample_CGAGGC.rem.1.fq.gz HEMI_LT_3080_CGAGGC.rem.1.fq.gz
mv sample_TCGCAT.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TCGCAT.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ACCAGA.rem.2.fq.gz
mv sample_CGAGGC.rem.2.fq.gz HEMI_LT_3080_CGAGGC.rem.2.fq.gz
mv sample_TCGCAT.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TCGCAT.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ACTTCC.1.fq.gz
mv sample_CTGGTT.1.fq.gz HEMI_LT_3082_CTGGTT.1.fq.gz
mv sample_TGCTTG.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TGCTTG.1.fq.gz
mv sample_ACTTCC.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ACTTCC.2.fq.gz
mv sample_CTGGTT.2.fq.gz HEMI_LT_3082_CTGGTT.2.fq.gz
mv sample_TGCTTG.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TGCTTG.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ACTTCC.rem.1.fq.gz
mv sample_CTGGTT.rem.1.fq.gz HEMI_LT_3082_CTGGTT.rem.1.fq.gz
mv sample_TGCTTG.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TGCTTG.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ACTTCC.rem.2.fq.gz
mv sample_CTGGTT.rem.2.fq.gz HEMI_LT_3082_CTGGTT.rem.2.fq.gz
mv sample_TGCTTG.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TGCTTG.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz HEMI_LT_3098_ATCGTC.1.fq.gz
mv sample_GATTAC.1.fq.gz HEMI_LT_3099_GATTAC.1.fq.gz
mv sample_TGGATT.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TGGATT.1.fq.gz
mv sample_ATCGTC.2.fq.gz HEMI_LT_3098_ATCGTC.2.fq.gz
mv sample_GATTAC.2.fq.gz HEMI_LT_3099_GATTAC.2.fq.gz
mv sample_TGGATT.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TGGATT.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz HEMI_LT_3098_ATCGTC.rem.1.fq.gz
mv sample_GATTAC.rem.1.fq.gz HEMI_LT_3099_GATTAC.rem.1.fq.gz
mv sample_TGGATT.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TGGATT.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz HEMI_LT_3098_ATCGTC.rem.2.fq.gz
mv sample_GATTAC.rem.2.fq.gz HEMI_LT_3099_GATTAC.rem.2.fq.gz
mv sample_TGGATT.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TGGATT.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ATTCAT.1.fq.gz
mv sample_GCCTGG.1.fq.gz HEMI_LT_3072_GCCTGG.1.fq.gz
mv sample_TTACGG.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TTACGG.1.fq.gz
mv sample_ATTCAT.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ATTCAT.2.fq.gz
mv sample_GCCTGG.2.fq.gz HEMI_LT_3072_GCCTGG.2.fq.gz
mv sample_TTACGG.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TTACGG.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ATTCAT.rem.1.fq.gz
mv sample_GCCTGG.rem.1.fq.gz HEMI_LT_3072_GCCTGG.rem.1.fq.gz
mv sample_TTACGG.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TTACGG.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_ATTCAT.rem.2.fq.gz
mv sample_GCCTGG.rem.2.fq.gz HEMI_LT_3072_GCCTGG.rem.2.fq.gz
mv sample_TTACGG.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TTACGG.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz HEMI_LT_3062_CAACCG.1.fq.gz
mv sample_GGAGCG.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GGAGCG.1.fq.gz
mv sample_TTGAAC.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TTGAAC.1.fq.gz
mv sample_CAACCG.2.fq.gz HEMI_LT_3062_CAACCG.2.fq.gz
mv sample_GGAGCG.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GGAGCG.2.fq.gz
mv sample_TTGAAC.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TTGAAC.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz HEMI_LT_3062_CAACCG.rem.1.fq.gz
mv sample_GGAGCG.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GGAGCG.rem.1.fq.gz
mv sample_TTGAAC.rem.1.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TTGAAC.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz HEMI_LT_3062_CAACCG.rem.2.fq.gz
mv sample_GGAGCG.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_GGAGCG.rem.2.fq.gz
mv sample_TTGAAC.rem.2.fq.gz Pool2_SEH_EW_ATCGAA_S16_L002_TTGAAC.rem.2.fq.gz

mv process_radtags.log Pool2_SEH_EW_ATCGAA_S16_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_12
module load stacks

process_radtags -1 Pool2_SEH_EW_CGGTTA_S14_L002_R1_001.fastq.gz -2 Pool2_SEH_EW_CGGTTA_S14_L002_R2_001.fastq.gz -o ./Pool2_SEH_EW_CGGTTA_S14_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool2_SEH_EW_CGGTTA_S14_samples/

mv sample_AACGAA.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_AACGAA.1.fq.gz
mv sample_CATCAA.1.fq.gz HEMI_LT_3074_CATCAA.1.fq.gz
mv sample_GGTAGA.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GGTAGA.1.fq.gz
mv sample_AACGAA.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_AACGAA.2.fq.gz
mv sample_CATCAA.2.fq.gz HEMI_LT_3074_CATCAA.2.fq.gz
mv sample_GGTAGA.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GGTAGA.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz _AACGAA.rem.1.fq.gz
mv sample_CATCAA.rem.1.fq.gz HEMI_LT_3074_CATCAA.rem.1.fq.gz
mv sample_GGTAGA.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GGTAGA.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_AACGAA.rem.2.fq.gz
mv sample_CATCAA.rem.2.fq.gz HEMI_LT_3074_CATCAA.rem.2.fq.gz
mv sample_GGTAGA.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GGTAGA.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_AACTCG.1.fq.gz
mv sample_CCAGCT.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_CCAGCT.1.fq.gz
mv sample_GGTCTT.1.fq.gz HEMI_LT_1122_GGTCTT.1.fq.gz
mv sample_AACTCG.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_AACTCG.2.fq.gz
mv sample_CCAGCT.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_CCAGCT.2.fq.gz
mv sample_GGTCTT.2.fq.gz HEMI_LT_1122_GGTCTT.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_AACTCG.rem.1.fq.gz
mv sample_CCAGCT.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_CCAGCT.rem.1.fq.gz
mv sample_GGTCTT.rem.1.fq.gz HEMI_LT_1122_GGTCTT.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_AACTCG.rem.2.fq.gz
mv sample_CCAGCT.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_CCAGCT.rem.2.fq.gz
mv sample_GGTCTT.rem.2.fq.gz HEMI_LT_1122_GGTCTT.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz HEMI_LT_3069_AAGATA.1.fq.gz
mv sample_CCGACC.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_CCGACC.1.fq.gz
mv sample_GTATGA.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GTATGA.1.fq.gz
mv sample_AAGATA.2.fq.gz HEMI_LT_3069_AAGATA.2.fq.gz
mv sample_CCGACC.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_CCGACC.2.fq.gz
mv sample_GTATGA.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GTATGA.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz HEMI_LT_3069_AAGATA.rem.1.fq.gz
mv sample_CCGACC.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_CCGACC.rem.1.fq.gz
mv sample_GTATGA.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GTATGA.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz HEMI_LT_3069_AAGATA.rem.2.fq.gz
mv sample_CCGACC.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_CCGACC.rem.2.fq.gz
mv sample_GTATGA.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GTATGA.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ACCAGA.1.fq.gz
mv sample_CGAGGC.1.fq.gz HEMI_LT_3075_CGAGGC.1.fq.gz
mv sample_TCGCAT.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TCGCAT.1.fq.gz
mv sample_ACCAGA.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ACCAGA.2.fq.gz
mv sample_CGAGGC.2.fq.gz HEMI_LT_3075_CGAGGC.2.fq.gz
mv sample_TCGCAT.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TCGCAT.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ACCAGA.rem.1.fq.gz
mv sample_CGAGGC.rem.1.fq.gz HEMI_LT_3075_CGAGGC.rem.1.fq.gz
mv sample_TCGCAT.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TCGCAT.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ACCAGA.rem.2.fq.gz
mv sample_CGAGGC.rem.2.fq.gz HEMI_LT_3075_CGAGGC.rem.2.fq.gz
mv sample_TCGCAT.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TCGCAT.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ACTTCC.1.fq.gz
mv sample_CTGGTT.1.fq.gz HEMI_LT_3066_CTGGTT.1.fq.gz
mv sample_TGCTTG.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TGCTTG.1.fq.gz
mv sample_ACTTCC.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ACTTCC.2.fq.gz
mv sample_CTGGTT.2.fq.gz HEMI_LT_3066_CTGGTT.2.fq.gz
mv sample_TGCTTG.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TGCTTG.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ACTTCC.rem.1.fq.gz
mv sample_CTGGTT.rem.1.fq.gz HEMI_LT_3066_CTGGTT.rem.1.fq.gz
mv sample_TGCTTG.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TGCTTG.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ACTTCC.rem.2.fq.gz
mv sample_CTGGTT.rem.2.fq.gz HEMI_LT_3066_CTGGTT.rem.2.fq.gz
mv sample_TGCTTG.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TGCTTG.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz HEMI_LT_3068_ATCGTC.1.fq.gz
mv sample_GATTAC.1.fq.gz HEMI_LT_3096_GATTAC.1.fq.gz
mv sample_TGGATT.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TGGATT.1.fq.gz
mv sample_ATCGTC.2.fq.gz HEMI_LT_3068_ATCGTC.2.fq.gz
mv sample_GATTAC.2.fq.gz HEMI_LT_3096_GATTAC.2.fq.gz
mv sample_TGGATT.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TGGATT.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz HEMI_LT_3068_ATCGTC.rem.1.fq.gz
mv sample_GATTAC.rem.1.fq.gz HEMI_LT_3096_GATTAC.rem.1.fq.gz
mv sample_TGGATT.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TGGATT.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz HEMI_LT_3068_ATCGTC.rem.2.fq.gz
mv sample_GATTAC.rem.2.fq.gz HEMI_LT_3096_GATTAC.rem.2.fq.gz
mv sample_TGGATT.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TGGATT.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ATTCAT.1.fq.gz
mv sample_GCCTGG.1.fq.gz HEMI_LT_3083_GCCTGG.1.fq.gz
mv sample_TTACGG.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TTACGG.1.fq.gz
mv sample_ATTCAT.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ATTCAT.2.fq.gz
mv sample_GCCTGG.2.fq.gz HEMI_LT_3083_GCCTGG.2.fq.gz
mv sample_TTACGG.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TTACGG.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ATTCAT.rem.1.fq.gz
mv sample_GCCTGG.rem.1.fq.gz HEMI_LT_3083_GCCTGG.rem.1.fq.gz
mv sample_TTACGG.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TTACGG.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_ATTCAT.rem.2.fq.gz
mv sample_GCCTGG.rem.2.fq.gz HEMI_LT_3083_GCCTGG.rem.2.fq.gz
mv sample_TTACGG.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TTACGG.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz HEMI_LT_3091_CAACCG.1.fq.gz
mv sample_GGAGCG.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GGAGCG.1.fq.gz
mv sample_TTGAAC.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TTGAAC.1.fq.gz
mv sample_CAACCG.2.fq.gz HEMI_LT_3091_CAACCG.2.fq.gz
mv sample_GGAGCG.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GGAGCG.2.fq.gz
mv sample_TTGAAC.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TTGAAC.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz HEMI_LT_3091_CAACCG.rem.1.fq.gz
mv sample_GGAGCG.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GGAGCG.rem.1.fq.gz
mv sample_TTGAAC.rem.1.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TTGAAC.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz HEMI_LT_3091_CAACCG.rem.2.fq.gz
mv sample_GGAGCG.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_GGAGCG.rem.2.fq.gz
mv sample_TTGAAC.rem.2.fq.gz Pool2_SEH_EW_CGGTTA_S14_L002_TTGAAC.rem.2.fq.gz

mv process_radtags.log Pool2_SEH_EW_CGGTTA_S14_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_13
module load stacks

process_radtags -1 Pool2_SEH_EW_GCTACA_S10_L002_R1_001.fastq.gz -2 Pool2_SEH_EW_GCTACA_S10_L002_R2_001.fastq.gz -o ./Pool2_SEH_EW_GCTACA_S10_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool2_SEH_EW_GCTACA_S10_samples/

mv sample_TCGCAT.1.fq.gz HEMI_375_TCGCAT.1.fq.gz
mv sample_TCGCAT.2.fq.gz HEMI_375_TCGCAT.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz HEMI_375_TCGCAT.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz HEMI_375_TCGCAT.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz HEMI_380_TGGATT.1.fq.gz
mv sample_TGGATT.2.fq.gz HEMI_380_TGGATT.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz HEMI_380_TGGATT.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz HEMI_380_TGGATT.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz HEMI_383_TTACGG.1.fq.gz
mv sample_TTACGG.2.fq.gz HEMI_383_TTACGG.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz HEMI_383_TTACGG.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz HEMI_383_TTACGG.rem.2.fq.gz
mv sample_AACGAA.1.fq.gz HEMI_R_2358_AACGAA.1.fq.gz
mv sample_AACGAA.2.fq.gz HEMI_R_2358_AACGAA.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz HEMI_R_2358_AACGAA.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz HEMI_R_2358_AACGAA.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz HEMI_R_445_GGTAGA.1.fq.gz
mv sample_GGTAGA.2.fq.gz HEMI_R_445_GGTAGA.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz HEMI_R_445_GGTAGA.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz HEMI_R_445_GGTAGA.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz HEMI_R_972_CCAGCT.1.fq.gz
mv sample_CCAGCT.2.fq.gz HEMI_R_972_CCAGCT.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz HEMI_R_972_CCAGCT.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz HEMI_R_972_CCAGCT.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_AACTCG.1.fq.gz
mv sample_AACTCG.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_AACTCG.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_AACTCG.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_AACTCG.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_ACCAGA.1.fq.gz
mv sample_ACCAGA.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_ACCAGA.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_ACCAGA.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_ACCAGA.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_ATTCAT.1.fq.gz
mv sample_ATTCAT.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_ATTCAT.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_ATTCAT.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_ATTCAT.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_CCGACC.1.fq.gz
mv sample_CCGACC.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_CCGACC.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_CCGACC.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_CCGACC.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_GGAGCG.1.fq.gz
mv sample_GGAGCG.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_GGAGCG.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_GGAGCG.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_GGAGCG.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_TTGAAC.1.fq.gz
mv sample_TTGAAC.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_TTGAAC.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_TTGAAC.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz Pool2_SEH_EW_GCTACA_S10_L002_TTGAAC.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz NOO_1025.1.fq.gz
mv sample_AAGATA.2.fq.gz NOO_1025.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz NOO_1025.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz NOO_1025.rem.2.fq.gz
mv sample_GCCTGG.1.fq.gz NOO_1028.1.fq.gz
mv sample_GCCTGG.2.fq.gz NOO_1028.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz NOO_1028.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz NOO_1028.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz NOO_1029.1.fq.gz
mv sample_CAACCG.2.fq.gz NOO_1029.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz NOO_1029.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz NOO_1029.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz SWO_1473.1.fq.gz
mv sample_ACTTCC.2.fq.gz SWO_1473.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz SWO_1473.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz SWO_1473.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz SEO_1521.1.fq.gz
mv sample_TGCTTG.2.fq.gz SEO_1521.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz SEO_1521.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz SEO_1521.rem.2.fq.gz
mv sample_GTATGA.1.fq.gz SEO_1522.1.fq.gz
mv sample_GTATGA.2.fq.gz SEO_1522.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz SEO_1522.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz SEO_1522.rem.2.fq.gz
mv sample_GGTCTT.1.fq.gz SEO_1578.1.fq.gz
mv sample_GGTCTT.2.fq.gz SEO_1578.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz SEO_1578.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz SEO_1578.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz SEO_1582.1.fq.gz
mv sample_ATCGTC.2.fq.gz SEO_1582.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz SEO_1582.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz SEO_1582.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz SEO_1590.1.fq.gz
mv sample_GATTAC.2.fq.gz SEO_1590.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz SEO_1590.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz SEO_1590.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz SWO_1627.1.fq.gz
mv sample_CTGGTT.2.fq.gz SWO_1627.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz SWO_1627.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.g SWO_1627.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz SWO_1630.1.fq.gz
mv sample_CATCAA.2.fq.gz SWO_1630.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz SWO_1630.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz SWO_1630.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz SWO_1633.1.fq.gz
mv sample_CGAGGC.2.fq.gz SWO_1633.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz SWO_1633.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz SWO_1633.rem.2.fq.gz

mv process_radtags.log Pool2_SEH_EW_GCTACA_S10_process_radtags.log

cd ..
#END


#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_14
module load stacks

process_radtags -1 Pool2_SEH_EW_GTCTAG_S15_L002_R1_001.fastq.gz -2 Pool2_SEH_EW_GTCTAG_S15_L002_R2_001.fastq.gz -o ./Pool2_SEH_EW_GTCTAG_S15_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool2_SEH_EW_GTCTAG_S15_samples/

mv sample_AACGAA.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_AACGAA.1.fq.gz
mv sample_CATCAA.1.fq.gz HEMI_LT_3077_CATCAA.1.fq.gz
mv sample_GGTAGA.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GGTAGA.1.fq.gz
mv sample_AACGAA.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_AACGAA.2.fq.gz
mv sample_CATCAA.2.fq.gz HEMI_LT_3077_CATCAA.2.fq.gz
mv sample_GGTAGA.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GGTAGA.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_AACGAA.rem.1.fq.gz
mv sample_CATCAA.rem.1.fq.gz HEMI_LT_3077_CATCAA.rem.1.fq.gz
mv sample_GGTAGA.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GGTAGA.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_AACGAA.rem.2.fq.gz
mv sample_CATCAA.rem.2.fq.gz HEMI_LT_3077_CATCAA.rem.2.fq.gz
mv sample_GGTAGA.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GGTAGA.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_AACTCG.1.fq.gz
mv sample_CCAGCT.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_CCAGCT.1.fq.gz
mv sample_GGTCTT.1.fq.gz HEMI_LT_3088_GGTCTT.1.fq.gz
mv sample_AACTCG.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_AACTCG.2.fq.gz
mv sample_CCAGCT.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_CCAGCT.2.fq.gz
mv sample_GGTCTT.2.fq.gz HEMI_LT_3088_GGTCTT.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_AACTCG.rem.1.fq.gz
mv sample_CCAGCT.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_CCAGCT.rem.1.fq.gz
mv sample_GGTCTT.rem.1.fq.gz HEMI_LT_3088_GGTCTT.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_AACTCG.rem.2.fq.gz
mv sample_CCAGCT.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_CCAGCT.rem.2.fq.gz
mv sample_GGTCTT.rem.2.fq.gz HEMI_LT_3088_GGTCTT.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz HEMI_LT_3085_AAGATA.1.fq.gz
mv sample_CCGACC.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_CCGACC.1.fq.gz
mv sample_GTATGA.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GTATGA.1.fq.gz
mv sample_AAGATA.2.fq.gz HEMI_LT_3085_AAGATA.2.fq.gz
mv sample_CCGACC.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_CCGACC.2.fq.gz
mv sample_GTATGA.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GTATGA.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz HEMI_LT_3085_AAGATA.rem.1.fq.gz
mv sample_CCGACC.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_CCGACC.rem.1.fq.gz
mv sample_GTATGA.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GTATGA.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz HEMI_LT_3085_AAGATA.rem.2.fq.gz
mv sample_CCGACC.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_CCGACC.rem.2.fq.gz
mv sample_GTATGA.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GTATGA.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ACCAGA.1.fq.gz
mv sample_CGAGGC.1.fq.gz HEMI_LT_3078_CGAGGC.1.fq.gz
mv sample_TCGCAT.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TCGCAT.1.fq.gz
mv sample_ACCAGA.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ACCAGA.2.fq.gz
mv sample_CGAGGC.2.fq.gz HEMI_LT_3078_CGAGGC.2.fq.gz
mv sample_TCGCAT.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TCGCAT.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ACCAGA.rem.1.fq.gz
mv sample_CGAGGC.rem.1.fq.gz HEMI_LT_3078_CGAGGC.rem.1.fq.gz
mv sample_TCGCAT.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TCGCAT.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ACCAGA.rem.2.fq.gz
mv sample_CGAGGC.rem.2.fq.gz HEMI_LT_3078_CGAGGC.rem.2.fq.gz
mv sample_TCGCAT.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TCGCAT.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ACTTCC.1.fq.gz
mv sample_CTGGTT.1.fq.gz HEMI_LT_3076_CTGGTT.1.fq.gz
mv sample_TGCTTG.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TGCTTG.1.fq.gz
mv sample_ACTTCC.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ACTTCC.2.fq.gz
mv sample_CTGGTT.2.fq.gz HEMI_LT_3076_CTGGTT.2.fq.gz
mv sample_TGCTTG.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TGCTTG.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ACTTCC.rem.1.fq.gz
mv sample_CTGGTT.rem.1.fq.gz HEMI_LT_3076_CTGGTT.rem.1.fq.gz
mv sample_TGCTTG.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TGCTTG.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ACTTCC.rem.2.fq.gz
mv sample_CTGGTT.rem.2.fq.gz HEMI_LT_3076_CTGGTT.rem.2.fq.gz
mv sample_TGCTTG.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TGCTTG.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz HEMI_LT_3063_ATCGTC.1.fq.gz
mv sample_GATTAC.1.fq.gz HEMI_LT_3064_GATTAC.1.fq.gz
mv sample_TGGATT.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TGGATT.1.fq.gz
mv sample_ATCGTC.2.fq.gz HEMI_LT_3063_ATCGTC.2.fq.gz
mv sample_GATTAC.2.fq.gz HEMI_LT_3064_GATTAC.2.fq.gz
mv sample_TGGATT.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TGGATT.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz HEMI_LT_3063_ATCGTC.rem.1.fq.gz
mv sample_GATTAC.rem.1.fq.gz HEMI_LT_3064_GATTAC.rem.1.fq.gz
mv sample_TGGATT.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TGGATT.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz HEMI_LT_3063_ATCGTC.rem.2.fq.gz
mv sample_GATTAC.rem.2.fq.gz HEMI_LT_3064_GATTAC.rem.2.fq.gz
mv sample_TGGATT.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TGGATT.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ATTCAT.1.fq.gz
mv sample_GCCTGG.1.fq.gz HEMI_LT_3086_GCCTGG.1.fq.gz
mv sample_TTACGG.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TTACGG.1.fq.gz
mv sample_ATTCAT.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ATTCAT.2.fq.gz
mv sample_GCCTGG.2.fq.gz HEMI_LT_3086_GCCTGG.2.fq.gz
mv sample_TTACGG.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TTACGG.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ATTCAT.rem.1.fq.gz
mv sample_GCCTGG.rem.1.fq.gz HEMI_LT_3086_GCCTGG.rem.1.fq.gz
mv sample_TTACGG.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TTACGG.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_ATTCAT.rem.2.fq.gz
mv sample_GCCTGG.rem.2.fq.gz HEMI_LT_3086_GCCTGG.rem.2.fq.gz
mv sample_TTACGG.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TTACGG.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz HEMI_LT_3089_CAACCG.1.fq.gz
mv sample_GGAGCG.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GGAGCG.1.fq.gz
mv sample_TTGAAC.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TTGAAC.1.fq.gz
mv sample_CAACCG.2.fq.gz HEMI_LT_3089_CAACCG.2.fq.gz
mv sample_GGAGCG.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GGAGCG.2.fq.gz
mv sample_TTGAAC.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TTGAAC.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz HEMI_LT_3089_CAACCG.rem.1.fq.gz
mv sample_GGAGCG.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GGAGCG.rem.1.fq.gz
mv sample_TTGAAC.rem.1.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TTGAAC.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz HEMI_LT_3089_CAACCG.rem.2.fq.gz
mv sample_GGAGCG.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_GGAGCG.rem.2.fq.gz
mv sample_TTGAAC.rem.2.fq.gz Pool2_SEH_EW_GTCTAG_S15_L002_TTGAAC.rem.2.fq.gz

mv process_radtags.log Pool2_SEH_EW_GTCTAG_S15_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_15
module load stacks

process_radtags -1 Pool2_SEH_EW_TAGTGC_S9_L002_R1_001.fastq.gz -2 Pool2_SEH_EW_TAGTGC_S9_L002_R2_001.fastq.gz -o ./Pool2_SEH_EW_TAGTGC_S9_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool2_SEH_EW_TAGTGC_S9_samples/

mv sample_TCGCAT.1.fq.gz HEMI_376_TCGCAT.1.fq.gz
mv sample_TCGCAT.2.fq.gz HEMI_376_TCGCAT.2.fq.gz
mv sample_TCGCAT.rem.1.fq.gz HEMI_376_TCGCAT.rem.1.fq.gz
mv sample_TCGCAT.rem.2.fq.gz HEMI_376_TCGCAT.rem.2.fq.gz
mv sample_TTACGG.1.fq.gz HEMI_377_TTACGG.1.fq.gz
mv sample_TTACGG.2.fq.gz HEMI_377_TTACGG.2.fq.gz
mv sample_TTACGG.rem.1.fq.gz HEMI_377_TTACGG.rem.1.fq.gz
mv sample_TTACGG.rem.2.fq.gz HEMI_377_TTACGG.rem.2.fq.gz
mv sample_TGGATT.1.fq.gz HEMI_381_TGGATT.1.fq.gz
mv sample_TGGATT.2.fq.gz HEMI_381_TGGATT.2.fq.gz
mv sample_TGGATT.rem.1.fq.gz HEMI_381_TGGATT.rem.1.fq.gz
mv sample_TGGATT.rem.2.fq.gz HEMI_381_TGGATT.rem.2.fq.gz
mv sample_AACGAA.1.fq.gz HEMI_R_1110_AACGAA.1.fq.gz
mv sample_AACGAA.2.fq.gz HEMI_R_1110_AACGAA.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz HEMI_R_1110_AACGAA.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz HEMI_R_1110_AACGAA.rem.2.fq.gz
mv sample_GGTAGA.1.fq.gz HEMI_R_1167_GGTAGA.1.fq.gz
mv sample_GGTAGA.2.fq.gz HEMI_R_1167_GGTAGA.2.fq.gz
mv sample_GGTAGA.rem.1.fq.gz HEMI_R_1167_GGTAGA.rem.1.fq.gz
mv sample_GGTAGA.rem.2.fq.gz HEMI_R_1167_GGTAGA.rem.2.fq.gz
mv sample_CCAGCT.1.fq.gz HEMI_R_961_CCAGCT.1.fq.gz
mv sample_CCAGCT.2.fq.gz HEMI_R_961_CCAGCT.2.fq.gz
mv sample_CCAGCT.rem.1.fq.gz HEMI_R_961_CCAGCT.rem.1.fq.gz
mv sample_CCAGCT.rem.2.fq.gz HEMI_R_961_CCAGCT.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_AACTCG.1.fq.gz
mv sample_AACTCG.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_AACTCG.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_AACTCG.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_AACTCG.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_ACCAGA.1.fq.gz
mv sample_ACCAGA.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_ACCAGA.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_ACCAGA.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_ACCAGA.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_ATTCAT.1.fq.gz
mv sample_ATTCAT.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_ATTCAT.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_ATTCAT.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_ATTCAT.rem.2.fq.gz
mv sample_CCGACC.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_CCGACC.1.fq.gz
mv sample_CCGACC.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_CCGACC.2.fq.gz
mv sample_CCGACC.rem.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_CCGACC.rem.1.fq.gz
mv sample_CCGACC.rem.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_CCGACC.rem.2.fq.gz
mv sample_GGAGCG.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_GGAGCG.1.fq.gz
mv sample_GGAGCG.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_GGAGCG.2.fq.gz
mv sample_GGAGCG.rem.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_GGAGCG.rem.1.fq.gz
mv sample_GGAGCG.rem.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_GGAGCG.rem.2.fq.gz
mv sample_TTGAAC.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_TTGAAC.1.fq.gz
mv sample_TTGAAC.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_TTGAAC.2.fq.gz
mv sample_TTGAAC.rem.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_TTGAAC.rem.1.fq.gz
mv sample_TTGAAC.rem.2.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002_TTGAAC.rem.2.fq.gz
mv sample_GGTCTT.1.fq.gz Pool2_SEH_EW_TAGTGC_S9_L002NOO_1534_GGTCTT.1.fq.gz
mv sample_GCCTGG.1.fq.gz NOO_0934.1.fq.gz
mv sample_GCCTGG.2.fq.gz NOO_0934.2.fq.gz
mv sample_GCCTGG.rem.1.fq.gz NOO_0934.rem.1.fq.gz
mv sample_GCCTGG.rem.2.fq.gz NOO_0934.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz SEO_0964.1.fq.gz
mv sample_CAACCG.2.fq.gz SEO_0964.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz SEO_0964.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz SEO_0964.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz SWO_1224.1.fq.gz
mv sample_ACTTCC.2.fq.gz SWO_1224.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz SWO_1224.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz SWO_1224.rem.2.fq.gz
mv sample_TGCTTG.1.fq.gz SWO_1225.1.fq.gz
mv sample_TGCTTG.2.fq.gz SWO_1225.2.fq.gz
mv sample_TGCTTG.rem.1.fq.gz SWO_1225.rem.1.fq.gz
mv sample_TGCTTG.rem.2.fq.gz SWO_1225.rem.2.fq.gz
mv sample_GTATGA.1.fq.gz SWO_1229.1.fq.gz
mv sample_GTATGA.2.fq.gz SWO_1229.2.fq.gz
mv sample_GTATGA.rem.1.fq.gz SWO_1229.rem.1.fq.gz
mv sample_GTATGA.rem.2.fq.gz SWO_1229.rem.2.fq.gz
mv sample_GGTCTT.2.fq.gz NOO_1534.2.fq.gz
mv sample_GGTCTT.rem.1.fq.gz NOO_1534.rem.1.fq.gz
mv sample_GGTCTT.rem.2.fq.gz NOO_1534.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz NOO_1575.1.fq.gz
mv sample_ATCGTC.2.fq.gz NOO_1575.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz NOO_1575.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz NOO_1575.rem.2.fq.gz
mv sample_GATTAC.1.fq.gz SEO_1577.1.fq.gz
mv sample_GATTAC.2.fq.gz SEO_1577.2.fq.gz
mv sample_GATTAC.rem.1.fq.gz SEO_1577.rem.1.fq.gz
mv sample_GATTAC.rem.2.fq.gz SEO_1577.rem.2.fq.gz
mv sample_CTGGTT.1.fq.gz SWO_1618.1.fq.gz
mv sample_CTGGTT.2.fq.gz SWO_1618.2.fq.gz
mv sample_CTGGTT.rem.1.fq.gz SWO_1618.rem.1.fq.gz
mv sample_CTGGTT.rem.2.fq.gz SWO_1618.rem.2.fq.gz
mv sample_CATCAA.1.fq.gz SWO_1623.1.fq.gz
mv sample_CATCAA.2.fq.gz SWO_1623.2.fq.gz
mv sample_CATCAA.rem.1.fq.gz SWO_1623.rem.1.fq.gz
mv sample_CATCAA.rem.2.fq.gz SWO_1623.rem.2.fq.gz
mv sample_CGAGGC.1.fq.gz SWO_1624.1.fq.gz
mv sample_CGAGGC.2.fq.gz SWO_1624.2.fq.gz
mv sample_CGAGGC.rem.1.fq.gz SWO_1624.rem.1.fq.gz
mv sample_CGAGGC.rem.2.fq.gz SWO_1624.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz WTD_P03_neg.1.fq.gz
mv sample_AAGATA.2.fq.gz WTD_P03_neg.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz WTD_P03_neg.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz WTD_P03_neg.rem.2.fq.gz

mv process_radtags.log Pool2_SEH_EW_TAGTGC_S9_process_radtags.log

cd ..
#END

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=8
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sarahhaworth@trentu.ca
################### Process_radtags_ddRAD1_16
module load stacks

process_radtags -1 Pool2_SEH_EW_TGTCAC_S13_L002_R1_001.fastq.gz -2 Pool2_SEH_EW_TGTCAC_S13_L002_R2_001.fastq.gz -o ./Pool2_SEH_EW_TGTCAC_S13_samples/ -b barcodes -e SbfI -E phred33 -r -c -q -i gzfastq

cd Pool2_SEH_EW_TGTCAC_S13_samples/

mv sample_AACGAA.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_AACGAA.1.fq.gz
mv sample_CATCAA.1.fq.gz HEMI_LT_3101_CATCAA.1.fq.gz
mv sample_GGTAGA.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GGTAGA.1.fq.gz
mv sample_AACGAA.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_AACGAA.2.fq.gz
mv sample_CATCAA.2.fq.gz HEMI_LT_3101_CATCAA.2.fq.gz
mv sample_GGTAGA.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GGTAGA.2.fq.gz
mv sample_AACGAA.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_AACGAA.rem.1.fq.gz
mv sample_CATCAA.rem.1.fq.gz HEMI_LT_3101_CATCAA.rem.1.fq.gz
mv sample_GGTAGA.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GGTAGA.rem.1.fq.gz
mv sample_AACGAA.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_AACGAA.rem.2.fq.gz
mv sample_CATCAA.rem.2.fq.gz HEMI_LT_3101_CATCAA.rem.2.fq.gz
mv sample_GGTAGA.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GGTAGA.rem.2.fq.gz
mv sample_AACTCG.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_AACTCG.1.fq.gz
mv sample_CCAGCT.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_CCAGCT.1.fq.gz
mv sample_GGTCTT.1.fq.gz HEMI_LT_3065_GGTCTT.1.fq.gz
mv sample_AACTCG.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_AACTCG.2.fq.gz
mv sample_CCAGCT.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_CCAGCT.2.fq.gz
mv sample_GGTCTT.2.fq.gz HEMI_LT_3065_GGTCTT.2.fq.gz
mv sample_AACTCG.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_AACTCG.rem.1.fq.gz
mv sample_CCAGCT.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_CCAGCT.rem.1.fq.gz
mv sample_GGTCTT.rem.1.fq.gz HEMI_LT_3065_GGTCTT.rem.1.fq.gz
mv sample_AACTCG.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_AACTCG.rem.2.fq.gz
mv sample_CCAGCT.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_CCAGCT.rem.2.fq.gz
mv sample_GGTCTT.rem.2.fq.gz HEMI_LT_3065_GGTCTT.rem.2.fq.gz
mv sample_AAGATA.1.fq.gz HEMI_LT_3081_AAGATA.1.fq.gz
mv sample_CCGACC.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_CCGACC.1.fq.gz
mv sample_GTATGA.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GTATGA.1.fq.gz
mv sample_AAGATA.2.fq.gz HEMI_LT_3081_AAGATA.2.fq.gz
mv sample_CCGACC.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_CCGACC.2.fq.gz
mv sample_GTATGA.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GTATGA.2.fq.gz
mv sample_AAGATA.rem.1.fq.gz HEMI_LT_3081_AAGATA.rem.1.fq.gz
mv sample_CCGACC.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_CCGACC.rem.1.fq.gz
mv sample_GTATGA.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GTATGA.rem.1.fq.gz
mv sample_AAGATA.rem.2.fq.gz HEMI_LT_3081_AAGATA.rem.2.fq.gz
mv sample_CCGACC.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_CCGACC.rem.2.fq.gz
mv sample_GTATGA.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GTATGA.rem.2.fq.gz
mv sample_ACCAGA.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ACCAGA.1.fq.gz
mv sample_CGAGGC.1.fq.gz HEMI_LT_3097_CGAGGC.1.fq.gz
mv sample_TCGCAT.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TCGCAT.1.fq.gz
mv sample_ACCAGA.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ACCAGA.2.fq.gz
mv sample_CGAGGC.2.fq.gz HEMI_LT_3097_CGAGGC.2.fq.gz
mv sample_TCGCAT.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TCGCAT.2.fq.gz
mv sample_ACCAGA.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ACCAGA.rem.1.fq.gz
mv sample_CGAGGC.rem.1.fq.gz HEMI_LT_3097_CGAGGC.rem.1.fq.gz
mv sample_TCGCAT.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TCGCAT.rem.1.fq.gz
mv sample_ACCAGA.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ACCAGA.rem.2.fq.gz
mv sample_CGAGGC.rem.2.fq.gz HEMI_LT_3097_CGAGGC.rem.2.fq.gz
mv sample_TCGCAT.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TCGCAT.rem.2.fq.gz
mv sample_ACTTCC.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ACTTCC.1.fq.gz
mv sample_CTGGTT.1.fq.gz HEMI_LT_3100_CTGGTT.1.fq.gz
mv sample_TGCTTG.1.fq.gz HEMI_LT_P04_neg_TGCTTG.1.fq.gz
mv sample_ACTTCC.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ACTTCC.2.fq.gz
mv sample_CTGGTT.2.fq.gz HEMI_LT_3100_CTGGTT.2.fq.gz
mv sample_TGCTTG.2.fq.gz HEMI_LT_P04_neg_TGCTTG.2.fq.gz
mv sample_ACTTCC.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ACTTCC.rem.1.fq.gz
mv sample_CTGGTT.rem.1.fq.gz HEMI_LT_3100_CTGGTT.rem.1.fq.gz
mv sample_TGCTTG.rem.1.fq.gz HEMI_LT_P04_neg_TGCTTG.rem.1.fq.gz
mv sample_ACTTCC.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ACTTCC.rem.2.fq.gz
mv sample_CTGGTT.rem.2.fq.gz HEMI_LT_3100_CTGGTT.rem.2.fq.gz
mv sample_TGCTTG.rem.2.fq.gz HEMI_LT_P04_neg_TGCTTG.rem.2.fq.gz
mv sample_ATCGTC.1.fq.gz HEMI_LT_3094_ATCGTC.1.fq.gz
mv sample_GATTAC.1.fq.gz HEMI_LT_3073_GATTAC.1.fq.gz
mv sample_TGGATT.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TGGATT.1.fq.gz
mv sample_ATCGTC.2.fq.gz HEMI_LT_3094_ATCGTC.2.fq.gz
mv sample_GATTAC.2.fq.gz HEMI_LT_3073_GATTAC.2.fq.gz
mv sample_TGGATT.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TGGATT.2.fq.gz
mv sample_ATCGTC.rem.1.fq.gz HEMI_LT_3094_ATCGTC.rem.1.fq.gz
mv sample_GATTAC.rem.1.fq.gz HEMI_LT_3073_GATTAC.rem.1.fq.gz
mv sample_TGGATT.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TGGATT.rem.1.fq.gz
mv sample_ATCGTC.rem.2.fq.gz HEMI_LT_3094_ATCGTC.rem.2.fq.gz
mv sample_GATTAC.rem.2.fq.gz HEMI_LT_3073_GATTAC.rem.2.fq.gz
mv sample_TGGATT.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TGGATT.rem.2.fq.gz
mv sample_ATTCAT.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ATTCAT.1.fq.gz
mv sample_GCCTGG.1.fq.gz HEMI_LT_3071_GCCTGG.1.fq.gz
mv sample_TTACGG.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TTACGG.1.fq.gz
mv sample_ATTCAT.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ATTCAT.2.fq.gz
mv sample_GCCTGG.2.fq.gz HEMI_LT_3071_GCCTGG.2.fq.gz
mv sample_TTACGG.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TTACGG.2.fq.gz
mv sample_ATTCAT.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ATTCAT.rem.1.fq.gz
mv sample_GCCTGG.rem.1.fq.gz HEMI_LT_3071_GCCTGG.rem.1.fq.gz
mv sample_TTACGG.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TTACGG.rem.1.fq.gz
mv sample_ATTCAT.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_ATTCAT.rem.2.fq.gz
mv sample_GCCTGG.rem.2.fq.gz HEMI_LT_3071_GCCTGG.rem.2.fq.gz
mv sample_TTACGG.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TTACGG.rem.2.fq.gz
mv sample_CAACCG.1.fq.gz HEMI_LT_3092_CAACCG.1.fq.gz
mv sample_GGAGCG.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GGAGCG.1.fq.gz
mv sample_TTGAAC.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TTGAAC.1.fq.gz
mv sample_CAACCG.2.fq.gz HEMI_LT_3092_CAACCG.2.fq.gz
mv sample_GGAGCG.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GGAGCG.2.fq.gz
mv sample_TTGAAC.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TTGAAC.2.fq.gz
mv sample_CAACCG.rem.1.fq.gz HEMI_LT_3092_CAACCG.rem.1.fq.gz
mv sample_GGAGCG.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GGAGCG.rem.1.fq.gz
mv sample_TTGAAC.rem.1.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TTGAAC.rem.1.fq.gz
mv sample_CAACCG.rem.2.fq.gz HEMI_LT_3092_CAACCG.rem.2.fq.gz
mv sample_GGAGCG.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_GGAGCG.rem.2.fq.gz
mv sample_TTGAAC.rem.2.fq.gz Pool2_SEH_EW_TGTCAC_S13_L002_TTGAAC.rem.2.fq.gz

mv process_radtags.log Pool2_SEH_EW_TGTCAC_S13_process_radtags.log

cd ..
#END
























