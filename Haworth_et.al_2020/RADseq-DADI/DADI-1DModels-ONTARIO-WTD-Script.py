"""
1D Models 
Run on Ontario WTD samples
"""

"""
Importing required modules within ipython
"""
import dadi
import numpy
from numpy import array
import vcf
import pylab
import matplotlib
matplotlib.use("AGG")
import demographics1pop

"""
Creating 1D SFS on Ontario WTD 
"""
dd = dadi.Misc.make_data_dict(filename="Ontario-WTD-SNPs-1D.txt")
fs = dadi.Spectrum.from_data_dict(dd, ['Ontario'], [164], polarized=False)

"""
Exporting SFS to file
dadi.Spectrum.to_file(fs, "pop3_SFS.fs") 
"""

"""
Popgen Calculations Fst and S
dadi.Spectrum.Fst(self=fs)
dadi.Spectrum.S(fs)
"""


"""
1D Models 
Run on Ontario WTD as a whole
"""

"""
-----------------------------------------------------------------------------
MODEL 1 = BOTTLEPLUSGROWTH
params = (nuB,nuF,T)
-----------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1])
upper_bound = [300]
lower_bound = [0.0001]


"""
Function used to execute these calculations
"""
func = demographics1pop.bottleplusgrowth 

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('Model_pop1_Ontario_bottleplusgrowth_1Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('Model_pop1_Ontario_bottleplusgrowth_1Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)

"""
-----------------------------------------------------------------------------
MODEL 2 = GROWTH
params = (nu,T)
-----------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1])
upper_bound = [300]
lower_bound = [0.0001]


"""
Function used to execute these calculations
"""
func = demographics1pop.growth

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('Model_pop1_Ontario_growth_1Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('Model_pop1_Ontario_growth_1Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)

"""
-----------------------------------------------------------------------------
MODEL 3 = SNM
params = notused
-----------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1,1])
upper_bound = [300]
lower_bound = [0.0001]


"""
Function used to execute these calculations
"""
func = demographics1pop.snm

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('Model_pop1_Ontario_snm_1Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('Model_pop1_Ontario_snm_1Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)

"""
-----------------------------------------------------------------------------
MODEL 4 = BOTTLEPOP
params = (nu,T)
-----------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1,1])
upper_bound = [300]
lower_bound = [0.0001]


"""
Function used to execute these calculations
"""
func = demographics1pop.bottlepop 

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('Model_pop1_Ontario_bottlepop_1Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('Model_pop1_Ontario_bottlepop_1Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)

"""
-----------------------------------------------------------------------------
MODEL 5 = TWOPOPCHANGES
params = (nuB,nuF,TB,TF)
-----------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1])
upper_bound = [300]
lower_bound = [0.0001]


"""
Function used to execute these calculations
"""
func = demographics1pop.twopopchanges

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('Model_pop1_Ontario_twopopchanges_1Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('Model_pop1_Ontario_twopopchanges_1Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)

"""
-----------------------------------------------------------------------------
MODEL 6 = GROWTHPLUSBOTTLE
params = (nuB,nuF,TB,TF)
-----------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1])
upper_bound = [300]
lower_bound = [0.0001]


"""
Function used to execute these calculations
"""
func = demographics1pop.growthplusbottle

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('Model_pop1_Ontario_growthplusbottle_1Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('Model_pop1_Ontario_growthplusbottle_1Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)

"""
"""
