"""
2D Models 
Run on North + Southern WTD as distinct groups

ipython --pylab
"""

"""
Importing required modules within ipython
"""
import dadi
import numpy
from numpy import array
import vcf
import pylab
import matplotlib
matplotlib.use("AGG")
import demographics2pop

"""
Creating SFS
"""
dd = dadi.Misc.make_data_dict(filename="Ontario-WTD-SNPs-2D-NvS.txt")
fs = dadi.Spectrum.from_data_dict(dd, ['Northern','Southern'], [108,236], polarized=False)

"""
-------------------------------------------------------------------------------------------------
MODEL 1 - STANDARD NEUTRAL MODEL (SNM)
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1,1])
upper_bound = [600]
lower_bound = [0.0001]


"""
Function used to execute these calculations
"""
func = demographics2pop.snm

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_snm_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_snm_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)


"""
-------------------------------------------------------------------------------------------------
MODEL 2 - BOTTLEGROWTH
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1])
upper_bound = [600,600,600]
lower_bound = [0.0001,0.0001,0.0001]

"""
Function used to execute these calculations
"""
func = demographics2pop.bottlegrowth

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_bottlegrowth_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_bottlegrowth_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)



"""
-------------------------------------------------------------------------------------------------
MODEL 3 - BOTTLEGROWTH_SPLIT
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1])
upper_bound = [600,600,600,600]
lower_bound = [0.0001,0.0001,0.0001,0.0001]

"""
Function used to execute these calculations
"""
func = demographics2pop.bottlegrowth_split

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_bottlegrowthsplit_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_bottlegrowthsplit_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)


"""
-------------------------------------------------------------------------------------------------
MODEL 4 - BOTTLEGROWTH_SPLIT_MIG
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1, 1])
upper_bound = [600,600,600,600,600]
lower_bound = [0.0001,0.0001,0.0001,0.0001,0.0001]

"""
Function used to execute these calculations
"""
func = demographics2pop.bottlegrowth_split_mig

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_bottlegrowthsplitmig_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_bottlegrowthsplitmig_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)

"""
-------------------------------------------------------------------------------------------------
MODEL 5 - SPLIT_MIG
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1])
upper_bound = [600,600,600,600]
lower_bound = [0.0001,0.0001,0.0001,0.0001]

"""
Function used to execute these calculations
"""
func = demographics2pop.split_mig

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_splitmig_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_splitmig_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)



"""
-------------------------------------------------------------------------------------------------
MODEL 6 - SPLIT_MIG_MSCORE
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1])
upper_bound = [600,600,600,600]
lower_bound = [0.0001,0.0001,0.0001,0.0001]

"""
Function used to execute these calculations
"""
func = demographics2pop.split_mig_mscore

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_splitmigscore_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_splitmigscore_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)

"""
-------------------------------------------------------------------------------------------------
MODEL 6 - IM
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1, 1, 1])
upper_bound = [600,600,600,600,600,600]
lower_bound = [0.0001,0.0001,0.0001,0.0001,0.0001,0.0001]

"""
Function used to execute these calculations
"""
func = demographics2pop.IM

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_IM_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_IM_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)

"""
-------------------------------------------------------------------------------------------------
MODEL 7 - IM_MSCORE
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1, 1, 1])
upper_bound = [600,600,600,600,600,600]
lower_bound = [0.0001,0.0001,0.0001,0.0001,0.0001,0.0001]

"""
Function used to execute these calculations
"""
func = demographics2pop.IM_mscore

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_IMmscore_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_IMmscore_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)


"""
-------------------------------------------------------------------------------------------------
MODEL 7 - IM_PRE
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1, 1, 1, 1, 1])
upper_bound = [600,600,600,600,600,600,600,600]
lower_bound = [0.0001,0.0001,0.0001,0.0001,0.0001,0.0001,0.0001,0.0001]

"""
Function used to execute these calculations
"""
func = demographics2pop.IM_pre

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_IMpre_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_IMpre_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)


"""
-------------------------------------------------------------------------------------------------
MODEL 8 - IM_PRE_MSCORE
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1, 1, 1, 1, 1])
upper_bound = [600,600,600,600,600,600,600,600]
lower_bound = [0.0001,0.0001,0.0001,0.0001,0.0001,0.0001,0.0001,0.0001]

"""
Function used to execute these calculations
"""
func = demographics2pop.IM_pre_mscore

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_IMpremscore_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_IMpremscore_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)


"""
-------------------------------------------------------------------------------------------------
MODEL 8 - IM_FSC
-------------------------------------------------------------------------------------------------
"""

"""
Defining paramters
"""
ns = fs.sample_sizes
pts_l = [170,180,190]
params = array([1, 1, 1, 1, 1])
upper_bound = [600,600,600,600,600]
lower_bound = [0.0001,0.0001,0.0001,0.0001,0.0001]

"""
Function used to execute these calculations
"""
func = demographics2pop.IM_fsc

"""
Executing the function
"""
func_ex = dadi.Numerics.make_extrap_log_func(func)
model = func_ex(params, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, fs)
print('Model log-likelihood:', ll_model)
theta = dadi.Inference.optimal_sfs_scaling(model, fs)
p0 = dadi.Misc.perturb_params(params, fold=1, upper_bound=upper_bound)
popt = dadi.Inference.optimize_log(p0, fs, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
print('Optimized parameters', repr(popt))
model = func_ex(popt, ns, pts_l)
ll_opt = dadi.Inference.ll_multinom(model, fs)
print('Optimized log-likelihood:', ll_opt)

bootstraps = []
for ii in range(100):
    print(ii)
    bootstrap_data = fs.sample()
    popt = dadi.Inference.optimize_log(p0, bootstrap_data, func_ex, pts_l, 
                                   lower_bound=lower_bound,
                                   upper_bound=upper_bound,
                                   verbose=len(params),
                                   maxiter=None)
    print('Optimized parameters', repr(popt))
    model = func_ex(popt, ns, pts_l)
    ll = dadi.Inference.ll_multinom(model, bootstrap_data)
    print('Optimized log-likelihood:', ll)
    theta = dadi.Inference.optimal_sfs_scaling(model, bootstrap_data)
    model *= theta
    bootstraps.append([ll, theta, popt[0], popt[1]])
bootstraps = numpy.array(bootstraps)
numpy.savetxt('FiltVCF_Model_pop2_01_NOOSOO_IMfsc_2Dboots.npy', bootstraps)
bootstraps = numpy.loadtxt('FiltVCF_Model_pop2_01_NOOSOO_IMfsc_2Dboots.npy')

sigma_boot = numpy.std(bootstraps, axis=0)[1:]

print('Bootstrap uncertainties:', sigma_boot)
