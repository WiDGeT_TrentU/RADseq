## Sar E Haworth
## First created 2020-01-10
## Ontario white-tailed deer project
## Under Dr. ABA Shafer and Dr. JM Northrup
## -----------------------------------------------------------------------------


## Step 1: Demultiplex
Make a new directory 'HiSeq'


In 'HiSeq', add:
	1) subdirectories where demultiplexed files go
		i.e. the -o part of the script is the subdirectory you will make
	2) barcode <file>
	3) script


Modify the script: You will need to create a file with the conversion of the standard process_radtags output files (eg. sample_AACGAA.1.fq.gz) into your sample names given the first tag
For example:
	1) Your SEH_AATCCG samples will contain sample that are named sample_AACGAA.1.fq.gz
	2) You will have to rename these files into SEO_1507.1.fq.gz where SEO means the sample was from Southeastern ON and the sample ID is 1507
		The file to do this quicker is called template_processradtags_namechange.sh
		This is ordered by the barcode, so that you can do it more efficiently if you organize your excel spreadsheet by ascending barcode as well
			You will see mv sample_AACGAA.1.fq.gz .1.fq.gz
			Where you can insert your sample ID into the second half
				i.e. .1.fq.gz --> SEO_1507.1.fq.gz
			In SUBLIME text editor, you can select mutiple lines at the same time using CTRL+Click
			Each sample will have four files
			.1.fq.gz
			.2.fq.gz
			.rem.1.fq.gz
			.rem.2.fq.gz


Once you've modified your script and the barcodes file to match your samples and their barcodes you're ready to submit!


You can submit on Sharcnet/ComputeCanada/Graham using:
	sbatch 20200109_ddRAD_Haworth_01_demultiplex.sh


## -----------------------------------------------------------------------------
## Step 2: QA/QC
Now I would suggest pruning your .1.fq.gz and .2.fq.gz files (and also remove associated .rem files)
Anything that contains fewer reads than the others move them into a new directory called "size_exclude"


For example I obtained this information by:
    1) Line counting each file using a script 
            #!/bin/bash
            #SBATCH --time=0-05:00           # time (DD-HH:MM)
            #SBATCH --account=rrg-shaferab
            #SBATCH --output=%x_%j.out
            #SBATCH --mail-type=ALL
            #SBATCH --mail-user=sarahhaworth@trentu.ca
            ################### J_linecounts
            cd /home/sarh/projects/rrg-shaferab/sarh/20200115_ddRAD_Trial_12_Final_prunedbams/HiSeq/Pair-files
            for i in *.gz
            do
            echo -n $i$'\t' >> Pair-linecounts_01.txt
            zcat $i | wc -l >> Pair-linecounts_01.txt
            done
            #END
        
            #!/bin/bash
            #SBATCH --time=0-05:00           # time (DD-HH:MM)
            #SBATCH --account=rrg-shaferab
            #SBATCH --output=%x_%j.out
            #SBATCH --mail-type=ALL
            #SBATCH --mail-user=sarahhaworth@trentu.ca
            ################### J_linecounts
            cd /home/sarh/projects/rrg-shaferab/sarh/20200115_ddRAD_Trial_12_Final_prunedbams/HiSeq/Rem-files
            for i in *.gz
            do
            echo -n $i$'\t' >> Rem-linecounts_01.txt
            zcat $i | wc -l >> Rem-linecounts_01.txt
            done
            #END
        
        
    2) REMOVAL OF PAIR-END FILES BASED ON NUMBER OF SEQUENCES PRESENT
	       For pair-end files: 
	       a) take count of .1.fq.gz and divide by four (in column "Count" into column "Calc")
	       b) take count of .2.fq.gz and divide by four (in column "Count" into column "Calc")
	       c) organize by ascending values (in column "Calc")
	       d) There are 428 fq files (214 samples total)
		       mean: 1,887,145 sequences per sample
		       median: 1,028,224 sequences per sample
		       range: 13,388 to 11,290,026 sequences per sample


    3) Graph SAMPLE_ID vs your linecounts divided by 4
        Within RStudio I ran:
	        ## Importing Pair-end Data
	        data <- read.csv(file="pair-end_count.csv",
                        header = T,
                        sep = "\t")
	        ## Library
	        library(ggplot2)
	        ## Plotting
	        ggplot(data=data, aes(x=reorder(Abbrv, -Calc), y=Calc)) + geom_bar(stat="identity") + theme_classic() + theme(axis.text.x = element_text(angle = 90)) + scale_y_continuous(expand = c(0,0)) + xlab("Sample Identifier") + ylab("Number of Sequences")
	        ## Saving
	        ggsave(filename = "pair-end_linecounts.jpg", device = c("jpeg"), height = 17, width = 33, scale = 1.5)


    4) It was determined that the lower cut-off value was a divided four count of 197,728 sequences per sample
	    e) The cut-off has been decided that if the value in c) was less than 200,000 sequences per sample (actually 197,728 sequences per sample) then the samples were discarded from further analysis
		    number of removed samples: 24
		    number of included samples: 190

        mv NOO_1220.1.fq.gz NOO_1220.2.fq.gz NOO_1234.1.fq.gz NOO_1234.2.fq.gz NOO_1312.1.fq.gz NOO_1312.2.fq.gz NOO_1529.1.fq.gz NOO_1529.2.fq.gz SEO_1089.1.fq.gz SEO_1089.2.fq.gz SEO_1090.1.fq.gz SEO_1090.2.fq.gz SEO_1337.1.fq.gz SEO_1337.2.fq.gz SEO_1340.1.fq.gz SEO_1340.2.fq.gz SEO_1345.1.fq.gz SEO_1345.2.fq.gz SEO_1350.1.fq.gz SEO_1350.2.fq.gz SEO_1357.1.fq.gz SEO_1357.2.fq.gz SEO_1369.1.fq.gz SEO_1369.2.fq.gz SEO_1466.1.fq.gz SEO_1466.2.fq.gz SEO_1484.1.fq.gz SEO_1484.2.fq.gz SEO_1491.1.fq.gz SEO_1491.2.fq.gz SEO_1492.1.fq.gz SEO_1492.2.fq.gz SEO_1496.1.fq.gz SEO_1496.2.fq.gz SEO_1498.1.fq.gz SEO_1498.2.fq.gz SEO_1513.1.fq.gz SEO_1513.2.fq.gz SEO_1582.1.fq.gz SEO_1582.2.fq.gz SWO_1389.1.fq.gz SWO_1389.2.fq.gz SWO_1444.1.fq.gz SWO_1444.2.fq.gz SWO_1450.1.fq.gz SWO_1450.2.fq.gz SWO_1473.1.fq.gz SWO_1473.2.fq.gz /home/sarh/projects/rrg-shaferab/sarh/20200120_ddRAD_Trial_12_Final_prunedbams/HiSeq/Pair-files/size_excluded

 
    5) For rem-end files: REMOVAL OF REM-END FILES BASED ON PAIR-END FILE REMOVAL. Rem files were not removed, except for those that matched the above removed samples. 
	        There are 428 .rem.fq.gz files (214 samples total)
	        mean: 34,424 sequences per sample
	        median: 5,079 sequences per sample
	        range: 78 to 413,418 sequences
        mv NOO_1220.rem.1.fq.gz NOO_1220.rem.2.fq.gz NOO_1234.rem.1.fq.gz NOO_1234.rem.2.fq.gz NOO_1312.rem.1.fq.gz NOO_1312.rem.2.fq.gz NOO_1529.rem.1.fq.gz NOO_1529.rem.2.fq.gz SEO_1089.rem.1.fq.gz SEO_1089.rem.2.fq.gz SEO_1090.rem.1.fq.gz SEO_1090.rem.2.fq.gz SEO_1337.rem.1.fq.gz SEO_1337.rem.2.fq.gz SEO_1340.rem.1.fq.gz SEO_1340.rem.2.fq.gz SEO_1345.rem.1.fq.gz SEO_1345.rem.2.fq.gz SEO_1350.rem.1.fq.gz SEO_1350.rem.2.fq.gz SEO_1357.rem.1.fq.gz SEO_1357.rem.2.fq.gz SEO_1369.rem.1.fq.gz SEO_1369.rem.2.fq.gz SEO_1466.rem.1.fq.gz SEO_1466.rem.2.fq.gz SEO_1484.rem.1.fq.gz SEO_1484.rem.2.fq.gz SEO_1491.rem.1.fq.gz SEO_1491.rem.2.fq.gz SEO_1492.rem.1.fq.gz SEO_1492.rem.2.fq.gz SEO_1496.rem.1.fq.gz SEO_1496.rem.2.fq.gz SEO_1498.rem.1.fq.gz SEO_1498.rem.2.fq.gz SEO_1513.rem.1.fq.gz SEO_1513.rem.2.fq.gz SEO_1582.rem.1.fq.gz SEO_1582.rem.2.fq.gz SWO_1389.rem.1.fq.gz SWO_1389.rem.2.fq.gz SWO_1444.rem.1.fq.gz SWO_1444.rem.2.fq.gz SWO_1450.rem.1.fq.gz SWO_1450.rem.2.fq.gz SWO_1473.rem.1.fq.gz SWO_1473.rem.2.fq.gz /home/sarh/projects/rrg-shaferab/sarh/20200120_ddRAD_Trial_12_Final_prunedbams/HiSeq/Rem-files/size_excluded


## -----------------------------------------------------------------------------
## Step 3: Making Bams and Sams 
Make a new directory 'RadSeq'

Inside the 'RadSeq', add:
	1) genome.fasta 
	2) script
	3) You will need to put all of your .rem.1.fq.gz and .rem.2.fq.gz files into a folder 'Rem-files'
	4) You will need to put all of your .1.fz.gz and .2.fq.gz files into a folder 'Pair-files'
	5) You will need a subdirectory called 'Bams'
	6) You will need a subdirectory called 'Sams'
	7) You will need a subdirectory called 'final_bams'
	7) You will need a subdirectory called 'discarded_bams'


you can submit the script now
	sbatch ddRAD_MakeBams.sh


In 'HiSeq' zip subdirectories
	zip -r DeMultiplex_files.zip Pool1_SEH_*samples Pool2_SEH_EW_*samples


## -----------------------------------------------------------------------------
## Step 4: Using gstacks and populations from the stacks program
Make a new directory 'Stacks'


In 'Stacks', add:
	1) population maps (i.e. ID <TAB> POPNUMBER)
		ENSURE SAMPLES IN discarded_bams HAVE BEEN REMOVED FROM THIS MAP
	2) script
	3) You will need a subdirectory called 'final_bams' 
	4) copy (cp) the contents from RadSeq/final_bams/ into Stacks/final_bams/
	

## -----------------------------------------------------------------------------
## Step 5: RStudio
Make a new directory 'rstudio'

In 'rstudio', add:
    1) your populations.snps.vcf
        I rename them according to their directory names (i.e. pop1r090.populations.snps.vcf)
        then I move them from the 'Stacks' directory (i.e. inside of pop1_r080_H/) into 'rstudio' 
        now i gzip all of the files using
            gzip *.vcf
    2) Haworth_et.al_RADseq-RStudio.R
        found in the RADseq_RStudio folder here
        modify for your own purposes
    3) you should have a metadata.csv with all your relevant metadata


Within 'rstudio'

To use R within Sharcnet:
    ## Version selection
    module unload r intel gcc openmpi
    module load nixpkgs/16.09 
    module load gcc/7.3.0
    module load cuda/10.0.130
    module load r/3.5.1
    module load udunits
    module load gdal/2.2.1

    ## Go to /home/sarh/
    ## mkdir R_local 
    export R_LIBS=/home/sarh/R_local/
    ## Now can install packages within R
To initialize R:
    R
To quit R: 
    q()
    

What I do is copy and paste each line in the .R file into my terminal R to execute the code


## -----------------------------------------------------------------------------