## 2020 03 03
## Sar E Haworth
## RADseq Ontario White-tailed deer (Odocoileus virginianus) project
## --------------------------------------------------------------------------------------------------------------------------------------------
## I CANNOT UPLOAD MY VCF INTO GITLAB, IT IS TOO LARGE
## --------------------------------------------------------------------------------------------------------------------------------------------



## Once you have generated your VCF file, you can then move into generating a SFS
## Install easySFS from https://github.com/isaacovercast/easySFS

## VCF used is generated from stacks given 3 populations in Ontario 
## VCF used has an inclusion r=0.90

## Step 1: Identify values for projection
./easySFS.py -i pop3.r090.populations.snps.vcf -p popmap1.txt --preview
## the output will be in the format (N, S) 
	## N = number of samples in the projection and 
	## S = number of segregaging sites
## copy and paste the output into a spreadsheet and organize by descending order of S
## select the N for the largest S, in my example that is N=164, S=370

## Step 2: Generate SFS
./easySFS.py -i pop3.r090.populations.snps.vcf -p popmap1.txt -a -f --proj 164
## The SFS will be put into a directory 'Output' containing datadict.txt and two subdirectories
## Within, theree will be subdirectories 'dadi' and 'fastsimcoal2'

## --------------------------------------------------------------------------------------------------------------------------------------------
## I REPEATED THIS FOR THREE POPULATIONS TOO
## --------------------------------------------------------------------------------------------------------------------------------------------

## Step 1: Identify values for projection
./easySFS -i pop3.r090.populations.snps.vcf -p popmap3.txt --preview
## the output will be in the format (N, S) 
	## N = number of samples in the projection and 
	## S = number of segregaging sites
## copy and paste the output into a spreadsheet and organize by descending order of S
## select the N for the largest S for each population
## i.e. if you have three populations, there will be three N's below

## Step 2: Generate SFS
./easySFS.py -i pop3.r090.populations.snps.vcf -p popmap3.txt -a -f --proj 108,164,72
## The SFS will be put into a directory 'Output' containing datadict.txt and two subdirectories
## Within, theree will be subdirectories 'dadi' and 'fastsimcoal2'

## --------------------------------------------------------------------------------------------------------------------------------------------
## DADI
## --------------------------------------------------------------------------------------------------------------------------------------------










